
/**
 * This file adds some LIVE to the Theme Customizer live preview. To leverage
 * this, set your custom settings to 'postMessage' and then add your handling
 * here. Your javascript should grab settings from customizer controls, and
 * then make any necessary changes to the page using jQuery.
 *
 * @see https://codex.wordpress.org/Theme_Customization_API#Part_3:_Configure_Live_Preview_.28Optional.29
 */
$(window).scroll(function(){
	var top	= $(window).scrollTop();
	if(top>=30){
		$("header").addClass('header-scroll');
		$(".item-menu-header").addClass('item-scroll');
		$("#logo-gray").removeClass('displaynone');
		$("#logo-white").addClass('displaynone');

	}

	else
		if($("header").hasClass('header-scroll')){
			$("header").removeClass('header-scroll');
			$(".item-menu-header").removeClass('item-scroll');
			$("#logo-gray").addClass('displaynone');
			$("#logo-white").removeClass('displaynone');
		}
});

$(document).ready(function(){
	$('.sb-slick-content').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: false,
	  asNavFor: '.sb-slick-sync'
	});
	$('.sb-slick-sync').slick({
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  asNavFor: '.sb-slick-content',
	  dots: false,
	  centerMode: false,
	  arrows: false,
	  focusOnSelect: true,
	  infinite: true
	});
	$('.sb-cases-list').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  dots: true,
	  arrows: true
	});
	$('.sb-produtos-item').on('click',function(){
		$.each($('.sb-produtos-item').not('.slick-current'),function(){
			$(this).css('backgroundImage',$(this).data('imgactive'));
		});
		$('.sb-produtos-item.slick-current').css('backgroundImage',$(this).data('imginactive'))
	});

});
$('.menu-header').slicknav();