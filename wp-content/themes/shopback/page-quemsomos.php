<?php 

/*
*	Template Name: Quem somos
*/

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

$titulo_principal		= get_field('titulo_principal');
$descricao				= get_field('descricao');
$texto_botao			= get_field('texto_botao');
$texto_quem_somos		= get_field('texto_quem_somos');


?>
<?php get_template_part( 'global-templates/hero', 'none' ); ?>
<?php get_template_part( 'global-templates/landingslider', 'none' ); ?>


<div class="wrapper quem-somos" id="page-wrapper">

	<div class="text-center" id="content" tabindex="-1">

		<div class="row">

			<main class="site-main" id="main">
				<section id="quem-somos-banner" class="text-center">
					<h1><?php echo $titulo_principal; ?></h1>
					<p><?php echo $descricao; ?></p>
					<a class="btn btn-default btn-filled-orange" href="#fale" role="button"><?php echo $texto_botao; ?></a>
				</section>
				<section id="texto-quem-somos">
					<?php echo $texto_quem_somos; ?>
				</section>

			</main><!-- #main -->

		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
