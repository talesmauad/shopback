<?php /* Template Name: produtos */ ?>
<?php
get_header();
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
        // Start the loop.
		while ( have_posts() ) : the_post();
			?>
			<div class="row">
				<div class="col-sm-6 text-center">
					<div class="product-container">
						<img src="<?=get_field('(produtos)_ícone_incial')['url']?>" alt="" class="product-icon">
						<h2><?php the_field('(produtos)_titulo_inicial') ?></h2>
						<p><?php the_field('(produtos)_descrição_inicial') ?></p>
						<a class="btn btn-default btn-filled-orange" href="#fale" role="button">FALE CONOSCO</a>
					</div>
				</div>
				<div class="col-sm-6"><img class="auto-margin prod-top-img" src="<?=get_field('(produtos)_imagem_inicial')['url']?>"></div>
			</div>
			<?php
            // Include the page content template.
			get_template_part( 'content', 'page' );
		endwhile;
		?>
	</main><!-- .site-main -->
</div><!-- .content-area -->

<section class="sb-comofunciona" id="comofunciona">
	<h3 class="section-title"><?php the_field('(produtos)_como_funciona_titulo') ?></h3>
	<div class="col-sm-10 mx-auto">
		<div class="blur-slider">
			<?php if(get_field('(produtos)_como_funciona_imagem_1')['url']){?>
			<article class="active">
				<img src="<?=get_field('(produtos)_como_funciona_imagem_1')['url']?>">>
				<p class="instructions"><?php the_field('(produtos)_como_funciona_instrução_1') ?></p>
			</article>
			<?php } ?>
			<?php if(get_field('(produtos)_como_funciona_imagem_2')['url']){?>
			<article class="blur">
				<img src="<?=get_field('(produtos)_como_funciona_imagem_2')['url']?>">>
				<p class="instructions"><?php the_field('(produtos)_como_funciona_instrução_2') ?></p>
			</article>
			<?php } ?>
			<?php if(get_field('(produtos)_como_funciona_imagem_3')['url']){?>
			<article class="blur">
				<img src="<?=get_field('(produtos)_como_funciona_imagem_3')['url']?>">>
				<p class="instructions"><?php the_field('(produtos)_como_funciona_instrução_3') ?></p>
			</article>
			<?php } ?>
			<?php if(get_field('(produtos)_como_funciona_imagem_4')['url']){?>
			<article class="blur">
				<img src="<?=get_field('(produtos)_como_funciona_imagem_4')['url']?>">>
				<p class="instructions"><?php the_field('(produtos)_como_funciona_instrução_4') ?></p>
			</article>
			<?php } ?>
		</div>
		<a class="btn btn-default btn-filled-orange" href="#fale" role="button">FALE CONOSCO</a>
	</div>
</section>

<section class="sb-produtos produtos-inner" id="produtos">
	<h3 class="section-title title-dark"><?php the_field('(produto)_titulo_features') ?></h3>
	<div class="row">
		<div class="col-sm-10 mx-auto">
			<div class="row hide-xs">
				<ul class="sb-produtos-list sb-slick-sync">
					<li class="sb-produtos-item feature1"><?php the_field('(produto)_nome_feature_1') ?></li>
					<li class="sb-produtos-item feature2"><?php the_field('(produto)_nome_feature_2') ?></li>
					<li class="sb-produtos-item feature3"><?php the_field('(produto)_nome_feature_3') ?></li>
					<li class="sb-produtos-item feature4"><?php the_field('(produto)_nome_feature_4') ?></li>
					<li class="sb-produtos-item feature5"><?php the_field('(produto)_nome_feature_5') ?></li>
				</ul>
			</div>
			<div class="sb-showcase" id="produtos-showcase">
				<div class="row">
					<div class="col-sm-12">
						<svg class="hide-xs bg-cutter" height="100%" width="100%">
							<polygon points="15,0 15,1000 340,0" style="fill:white;" />
						</svg>
						<div class="sb-slick-content">
							<div>
								<div class="row">
									<div class="col-sm-5 product-image">
										<img class="auto-margin inner-left" src="<?=get_field('(produto)_imagem_feature_1')['url']?>">
									</div>
									<div class="col-sm-7 text-left product-description">
										<img src="<?=get_field('(produto)_imagem_icone_feature_1')['url']?>" style="width: 50px;">
										<h4 class="prod-subtitle"><?php the_field('(produto)_nome_feature_1') ?></h4>
										<h3 class="gray"><?php the_field('(produto)_titulo_feature_1') ?></h3>
										<p class="gray"><?php the_field('(produto)_descrição_feature_1') ?></p>
									</div>
								</div>
							</div>
							<div>
								<div class="row">
									<div class="col-sm-5 product-image">
										<img class="auto-margin inner-left" src="<?=get_field('(produto)_imagem_feature_2')['url']?>">
									</div>
									<div class="col-sm-7 text-left product-description">
										<img src="<?=get_field('(produto)_imagem_icone_feature_2')['url']?>" style="width: 50px;">
										<h4 class="prod-subtitle"><?php the_field('(produto)_nome_feature_2') ?></h4>
										<h3 class="gray"><?php the_field('(produto)_titulo_feature_2') ?></h3>
										<p class="gray"><?php the_field('(produto)_descrição_feature_2') ?></p>
									</div>
								</div>
							</div>
							<div>
								<div class="row">
									<div class="col-sm-5 product-image">
										<img class="auto-margin inner-left" src="<?=get_field('(produto)_imagem_feature_3')['url']?>">
									</div>
									<div class="col-sm-7 text-left product-description">
										<img src="<?=get_field('(produto)_imagem_icone_feature_3')['url']?>" style="width: 50px;">
										<h4 class="prod-subtitle"><?php the_field('(produto)_nome_feature_3') ?></h4>
										<h3 class="gray"><?php the_field('(produto)_titulo_feature_3') ?></h3>
										<p class="gray"><?php the_field('(produto)_descrição_feature_3') ?></p>
									</div>
								</div>
							</div>
							<div>
								<div class="row">
									<div class="col-sm-5 product-image">
										<img class="auto-margin inner-left" src="<?=get_field('(produto)_imagem_feature_4')['url']?>">
									</div>
									<div class="col-sm-7 text-left product-description">
										<img src="<?=get_field('(produto)_imagem_icone_feature_4')['url']?>" style="width: 50px;">
										<h4 class="prod-subtitle"><?php the_field('(produto)_nome_feature_4') ?></h4>
										<h3 class="gray"><?php the_field('(produto)_titulo_feature_4') ?></h3>
										<p class="gray"><?php the_field('(produto)_descrição_feature_4') ?></p>
									</div>
								</div>
							</div>
							<div>
								<div class="row">
									<div class="col-sm-5 product-image">
										<img class="auto-margin inner-left" src="<?=get_field('(produto)_imagem_feature_5')['url']?>">
									</div>
									<div class="col-sm-7 text-left product-description">
										<img src="<?=get_field('(produto)_imagem_icone_feature_5')['url']?>" style="width: 50px;">
										<h4 class="prod-subtitle"><?php the_field('(produto)_nome_feature_5') ?></h4>
										<h3 class="gray"><?php the_field('(produto)_titulo_feature_5') ?></h3>
										<p class="gray"><?php the_field('(produto)_descrição_feature_5') ?>.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<p><a class="btn btn-default btn-filled-orange" href="#fale" role="button">FALE CONOSCO</a></p>
</section>


<section class="plataformas container-fluid">
	<div class="row">
		<div class="col-sm-10 mx-auto">
			<h4><?php the_field('(produtos)_titulo_plataformas') ?></h4> 
			<div class="row">
				<div class="col-xs-6 col-md-2 item-plataformas">
					<div class=" img-plataformas"> 
						<img  src="<?=get_field('(produtos)_plataformas_imagem_1')['url']?>">
					</div>
					<p><?php the_field('(produtos)_plataformas_texto_1') ?></p>
				</div>
				<div class="col-xs-6 col-md-2 item-plataformas">
					<div class=" img-plataformas"> 
						<img  src="<?=get_field('(produtos)_plataformas_imagem_2')['url']?>">
					</div>
					<p><?php the_field('(produtos)_plataformas_texto_2') ?></p>
				</div>
				<div class="col-xs-6 col-md-2 item-plataformas">
					<div class=" img-plataformas"> 
						<img  src="<?=get_field('(produtos)_plataformas_imagem_3')['url']?>">
					</div>
					<p><?php the_field('(produtos)_plataformas_texto_3') ?></p>
				</div>
				<div class="col-xs-6 col-md-2 item-plataformas">
					<div class=" img-plataformas"> 
						<img  src="<?=get_field('(produtos)_plataformas_imagem_4')['url']?>">
					</div>
					<p><?php the_field('(produtos)_plataformas_texto_4') ?></p>
				</div>
				<div class="col-xs-6 col-md-2 item-plataformas">
					<div class=" img-plataformas"> 
						<img  src="<?=get_field('(produtos)_plataformas_imagem_5')['url']?>">
					</div>
					<p><?php the_field('(produtos)_plataformas_texto_5') ?></p>
				</div>
				<div class="col-xs-6 col-md-2 item-plataformas">
					<div class=" img-plataformas"> 
						<img  src="<?=get_field('(produtos)_plataformas_imagem_6')['url']?>">
					</div>
					<p><?php the_field('(produtos)_plataformas_texto_6') ?></p>
				</div>
				<div class="col-xs-6 col-md-2 item-plataformas">
					<div class=" img-plataformas"> 
						<img  src="<?=get_field('(produtos)_plataformas_imagem_7')['url']?>">
					</div>
					<p><?php the_field('(produtos)_plataformas_texto_7') ?></p>
				</div>
				<div class="col-xs-6 col-md-2 item-plataformas">
					<div class=" img-plataformas"> 
						<img  src="<?=get_field('(produtos)_plataformas_imagem_8')['url']?>">
					</div>
					<p><?php the_field('(produtos)_plataformas_texto_8') ?></p>
				</div>
				<div class="col-xs-6 col-md-2 item-plataformas">
					<div class=" img-plataformas"> 
						<img  src="<?=get_field('(produtos)_plataformas_imagem_9')['url']?>">
					</div>
					<p><?php the_field('(produtos)_plataformas_texto_9') ?></p>
				</div>
				<div class="col-xs-6 col-md-2 item-plataformas">
					<div class=" img-plataformas"> 
						<img  src="<?=get_field('(produtos)_plataformas_imagem_10')['url']?>">
					</div>
					<p><?php the_field('(produtos)_plataformas_texto_10') ?></p>
				</div>
				<div class="col-xs-6 col-md-2 item-plataformas">
					<div class=" img-plataformas"> 
						<img  src="<?=get_field('(produtos)_plataformas_imagem_11')['url']?>">
					</div>
					<p><?php the_field('(produtos)_plataformas_texto_11') ?></p>
				</div>
				<div class="col-xs-6 col-md-2 item-plataformas">
					<div class=" img-plataformas"> 
						<img  src="<?=get_field('(produtos)_plataformas_imagem_12')['url']?>">
					</div>
					<p><?php the_field('(produtos)_plataformas_texto_12') ?></p>
				</div>

			</div>
		</div>
	</div>
</section>

<section class="sb-cases" id="clientes">
	<div class="sb-cases-list">		
		
		<div class="sb-cases-item" style="min-height: 300px; background-image: url(<?=get_field('(produto)_background_cases_de_sucesso_1')['url']?>);">
			<h3 class="section-title title-orange"><?php the_field('(produto)_cases_de_sucesso_titulo') ?></h3>
			<img src="<?=get_field('(produto)_imagem_cases_de_sucesso_1')['url']?>">
			<h1><?php the_field('(produto)_descrição_cases_de_sucesso_1') ?></h1>
			<p><br/><a class="btn btn-default btn-open-white" href="<?=get_field('(produto)_link_cases_de_sucesso_1')?>" target="_blank" role="button">+SAIBA MAIS</a></p>
		</div>
		<div class="sb-cases-item" style="min-height: 300px; background-image: url(<?=get_field('(produto)_background_cases_de_sucesso_2')['url']?>);">
			<h3 class="section-title title-orange"><?php the_field('(produto)_cases_de_sucesso_titulo') ?></h3>
			<img  src="<?=get_field('(produto)_imagem_cases_de_sucesso_2')['url']?>">
			<h1><?php the_field('(produto)_descrição_cases_de_sucesso_2') ?></h1>
			<p><br/><a class="btn btn-default btn-open-white" href="<?=get_field('(produto)_link_cases_de_sucesso_2')?>" target="_blank" role="button">+SAIBA MAIS</a></p>
		</div>
		<div class="sb-cases-item" style="min-height: 300px; background-image: url(<?=get_field('(produto)_background_cases_de_sucesso_3')['url']?>);">
			<h3 class="section-title title-orange"><?php the_field('(produto)_cases_de_sucesso_titulo') ?></h3>
			<img  src="<?=get_field('(produto)_imagem_cases_de_sucesso_3')['url']?>">
			<h1><?php the_field('(produto)_descrição_cases_de_sucesso_3') ?></h1>
			<p><br/><a class="btn btn-default btn-open-white" href="<?=get_field('(produto)_link_cases_de_sucesso_3')?>" target="_blank" role="button">+SAIBA MAIS</a></p>
		</div>
		<div class="sb-cases-item" style="min-height: 300px; background-image: url(<?=get_field('(produto)_background_cases_de_sucesso_4')['url']?>);">
			<h3 class="section-title title-orange"><?php the_field('(produto)_cases_de_sucesso_titulo') ?></h3>
			<img  src="<?=get_field('(produto)_imagem_cases_de_sucesso_4')['url']?>">
			<h1><?php the_field('(produto)_descrição_cases_de_sucesso_4') ?></h1>
			<p><br/><a class="btn btn-default btn-open-white" href="<?=get_field('(produto)_link_cases_de_sucesso_4')?>" target="_blank" role="button">+SAIBA MAIS</a></p>
		</div>
	</div>

</section>


<section class="sb-marcas-2" id="marcas2">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10 text-center">
			<ul class="sb-marcas-list">
				<li class="sb-marcas-item"><img src="<?=get_field('(produto)_faixa_propaganda_imagem_1')['url']?>"></li>
				<li class="sb-marcas-item"><img src="<?=get_field('(produto)_faixa_propaganda_imagem_2')['url']?>"</li>
				<li class="sb-marcas-item"><img src="<?=get_field('(produto)_faixa_propaganda_imagem_3')['url']?>"</li>
				<li class="sb-marcas-item"><img src="<?=get_field('(produto)_faixa_propaganda_imagem_4')['url']?>"</li>
				<li class="sb-marcas-item"><img src="<?=get_field('(produto)_faixa_propaganda_imagem_5')['url']?>"</li>
				<li class="sb-marcas-item"><img src="<?=get_field('(produto)_faixa_propaganda_imagem_6')['url']?>"</li>
				<li class="sb-marcas-item"><img src="<?=get_field('(produto)_faixa_propaganda_imagem_7')['url']?>"</li>
			</ul>
		</div>
		<div class="col-sm-1"></div>
	</div>
</section>

<section class="sb-quebracabeca quebra-produtos text-center" id="quebracabeca">
	<div class="row">
		<div class="col-sm-7 mx-auto">
			<h2><?php the_field('(produto)_propaganda_chamada') ?></h2>
			<a class="btn btn-default btn-through-white-dark btn-small" href="#fale" role="button">FALE CONOSCO</a>
		</div>
	</div>
</section>

<script src="<?=get_template_directory_uri()?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri()?>/js/slick.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.sb-slick-content').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: false,
			asNavFor: '.sb-slick-sync'
		});
		$('.sb-slick-sync').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			asNavFor: '.sb-slick-content',
			dots: false,
			centerMode: false,
			arrows: false,
			focusOnSelect: true,
			infinite: true
		});
		$('.sb-cases-list').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			arrows: true
		});

		var $container	= $('#comofunciona .blur-slider'),
		$articles	= $container.children('article'),
		timeout;

		$articles.on( 'mouseenter', function( event ) {

			var $article	= $(this);
			clearTimeout( timeout );
			timeout = setTimeout( function() {

				if( $article.hasClass('active') ) return false;

				$articles.not( $article.removeClass('blur').addClass('active') )
				.removeClass('active')
				.addClass('blur');

			}, 65 );

		});

		// $container.on( 'mouseleave', function( event ) {

		// 	clearTimeout( timeout );
		// 	$articles.removeClass('active blur');

		// });

		$('#primary').append('<svg class="hide-xs bg-cutter-products" height="50px" width="100%"><polygon points="0,50 '+window.innerWidth+',0 '+window.innerWidth+',50 0,50" style="fill:white;" /></svg>')
		window.onresize = function(event) {
			$('#primary .bg-cutter-products').remove()
			$('#primary').append('<svg class="hide-xs bg-cutter-products" height="50px" width="100%"><polygon points="0,50 '+window.innerWidth+',0 '+window.innerWidth+',50 0,50" style="fill:white;" /></svg>')
		};
	});
</script>
<style type="text/css">


.sb-produtos-item.feature1{ background-image: url(<?=get_field('(produto)_imagem_icone_laranja_feature_1')['url']?>) }
.sb-produtos-item.feature1:hover, .sb-produtos-item.feature1.slick-current{ background-image: url(<?=get_field('(produto)_imagem_icone_selecionado_feature_1')['url']?>) }
.sb-produtos-item.feature2{ background-image: url(<?=get_field('(produto)_imagem_icone_laranja_feature_2')['url']?>) }
.sb-produtos-item.feature2:hover, .sb-produtos-item.feature2.slick-current{ background-image: url(<?=get_field('(produto)_imagem_icone_selecionado_feature_2')['url']?>) }
.sb-produtos-item.feature3{ background-image:url(<?=get_field('(produto)_imagem_icone_laranja_feature_3')['url']?>) }
.sb-produtos-item.feature3:hover, .sb-produtos-item.feature3.slick-current{ background-image:  url(<?=get_field('(produto)_imagem_icone_selecionado_feature_3')['url']?>) }
.sb-produtos-item.feature4{ background-image: url(<?=get_field('(produto)_imagem_icone_laranja_feature_4')['url']?>) }
.sb-produtos-item.feature4:hover, .sb-produtos-item.feature4.slick-current{ background-image: url(<?=get_field('(produto)_imagem_icone_selecionado_feature_4')['url']?>) }
.sb-produtos-item.feature5{ background-image: url(<?=get_field('(produto)_imagem_icone_laranja_feature_5')['url']?>) }
.sb-produtos-item.feature5:hover, .sb-produtos-item.feature5.slick-current{ background-image:  url(<?=get_field('(produto)_imagem_icone_selecionado_feature_5')['url']?>) }


</style>

<?php get_footer(); ?>
