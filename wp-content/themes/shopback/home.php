<?php /* Template Name: Home */ ?>
<?php
get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero', 'none' ); ?>
	<?php get_template_part( 'global-templates/landingslider', 'none' ); ?>
<?php endif; ?>


<section class="sb-quemsomos" id="quemsomos">
	<div class="home-header-wrapper bg-dark sb-home">
		<div class="home-header-overlay">
			<div class="home-header-shadow">
				<div class="container">
					<div class="home-header-content text-center">
						<div class="row">
							<div class="col-sm-7 mx-auto sb-top-text">
								<h1 class="title text-center sb-main-title"><?php the_field('titulo_principal') ?></h1>
								<p class="subtitle text text-center sb-main-cta sans"><?php the_field('descrição') ?></p>
								<a class="btn btn-default btn-filled-orange" href="#fale" role="button">FALE CONOSCO</a><br/>
								<div class="sb-down-arrow"><i class="fa fa-angle-down"></i></div>
							</div>
						</div>
					</div>
				</div>
				<div class="home-header-down-arrow"><img class="img-responsive" src="<?=get_template_directory_uri()?>/img/down-arrow.png"></div>
			</div>
		</div>
	</div>
</section>

<section class="sb-marcas" id="marcas">
	<ul class="sb-marcas-list">
		<?php 
		$marcas = explode(',',get_field('lista_de_marcas'));
		foreach ($marcas as $item) {
			?><li class="sb-marcas-item"><img src="<?=get_template_directory_uri().'/'.$item?>"></li><?php
		}
		?>
	</ul>
</section>

<section class="sb-produtos" id="produtos">
	<h3 class="section-title title-dark"><?php the_field('titulo_principal_produtos') ?></h3>
	<div class="row">
		<div class="col-sm-10 mx-auto">
			<div class="row hide-xs">
				<ul class="sb-produtos-list sb-slick-sync">
					<li class="sb-produtos-item item1"><?php the_field('produtos_icone_1') ?></li>
					<li class="sb-produtos-item item2"><?php the_field('produtos_icone_2') ?></li>
					<li class="sb-produtos-item item3"><?php the_field('produtos_icone_3') ?></li>
					<li class="sb-produtos-item item4"><?php the_field('produtos_icone_4') ?></li>
					<li class="sb-produtos-item item5"><?php the_field('produtos_icone_5') ?></li>
				</ul>
			</div>
			<div class="sb-showcase" id="produtos-showcase">
				<div class="row">
					<div class="col-sm-12">
						<svg class="hide-xs bg-cutter" height="100%" width="100%">
							<polygon points="15,0 15,1000 340,0" style="fill:white;" />
						</svg>
						<div class="sb-slick-content">
							<div>
								<div class="row">
									<div class="col-sm-5 product-image"><img class="auto-margin" src="<?=get_field('home_imagem_produto_1')['url']?>"></div>
									<div class="col-sm-7 text-left product-description">
										<img src="<?=get_field('imagem_icone_produtos_1')['url']?>">
										<h4 class="sans"><?php the_field('titulo_produtos_1') ?></h4> 
										<h3 class="sans"><?php the_field('subtitulo_produtos_1') ?></h3> 
										<span class="sans"><?php the_field('descrição_produtos_1') ?></span>
										<p><br/><a class="btn btn-default btn-open-white" href="<?=get_field('link_produto_1')?>" target="_blank" role="button">+SAIBA MAIS</a></p> 
									</div>
								</div>
							</div>
							<div>
								<div class="row">
									<div class="col-sm-5 product-image"><img class="auto-margin" src="<?=get_field('home_imagem_produto_2')['url']?>"></div>
									<div class="col-sm-7 text-left product-description">
										<img src="<?=get_field('imagem_icone_produtos_2')['url']?>">
										<h4 class="sans"><?php the_field('titulo_produtos_2') ?></h4> 
										<h3 class="sans"><?php the_field('subtitulo_produtos_2') ?></h3> 
										<span class="sans"><?php the_field('descrição_produtos_2') ?></span>
										<p><br/><a class="btn btn-default btn-open-white" href="<?=get_field('link_produto_2')?>" target="_blank" role="button">+SAIBA MAIS</a></p>
									</div>
								</div>
							</div>
							<div>
								<div class="row">
									<div class="col-sm-5 product-image"><img class="auto-margin" src="<?=get_field('home_imagem_produto_3')['url']?>"></div>
									<div class="col-sm-7 text-left product-description">
										<img src="<?=get_field('imagem_icone_produtos_3')['url']?>">
										<h4 class="sans"><?php the_field('titulo_produtos_3') ?></h4> 
										<h3 class="sans"><?php the_field('subtitulo_produtos_3') ?></h3> 
										<span class="sans"><?php the_field('descrição_produtos_3') ?></span>
										<p><br/><a class="btn btn-default btn-open-white" href="<?=get_field('link_produto_3')?>" target="_blank" role="button">+SAIBA MAIS</a></p>
									</div>
								</div>
							</div>
							<div>
								<div class="row">
									<div class="col-sm-5 product-image"><img class="auto-margin" src="<?=get_field('home_imagem_produto_4')['url']?>"></div>
									<div class="col-sm-7 text-left product-description">
										<img src="<?=get_field('imagem_icone_produtos_4')['url']?>">
										<h4 class="sans"><?php the_field('titulo_produtos_4') ?></h4> 
										<h3 class="sans"><?php the_field('subtitulo_produtos_4') ?></h3> 
										<span class="sans"><?php the_field('descrição_produtos_4') ?></span>
										<p><br/><a class="btn btn-default btn-open-white" href="<?=get_field('link_produto_4')?>" target="_blank" role="button">+SAIBA MAIS</a></p>
									</div>
								</div>
							</div>
							<div>
								<div class="row">
									<div class="col-sm-5 product-image"><img class="auto-margin" src="<?=get_field('home_imagem_produto_5')['url']?>"></div>
									<div class="col-sm-7 text-left product-description">
										<img src="<?=get_field('imagem_icone_produtos_5')['url']?>">
										<h4 class="sans"><?php the_field('titulo_produtos_5') ?></h4> 
										<h3 class="sans"><?php the_field('subtitulo_produtos_5') ?></h3> 
										<span class="sans"><?php the_field('descrição_produtos_5') ?></span>
										<p><br/><a class="btn btn-default btn-open-white" href="<?=get_field('link_produto_5')?>" target="_blank" role="button">+SAIBA MAIS</a></p>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<p><a class="btn btn-default btn-filled-orange" href="#fale" role="button">FALE CONOSCO</a></p>
</section>

<section class="sb-cases" id="clientes">
	<div class="sb-cases-list">
		
		<div class="sb-cases-item" style="min-height: 300px; background-image: url(<?=get_field('bg_cases_de_sucesso_1')['url']?>);">
			<h3 class="section-title"><?php the_field('titulo_cases_sucesso') ?></h3>
			<img class="case-pic" src="<?=get_field('imagem_cases_de_sucesso_1')['url']?>">
			<h1><?php the_field('descrição_cases_de_sucesso_1') ?></h1> 
			<p><br/><a class="btn btn-default btn-open-white" href="<?=get_field('link_cases_de_sucesso_1')?>" target="_blank" role="button">+SAIBA MAIS</a></p>
		</div>
		<div class="sb-cases-item" style="min-height: 300px; background-image: url(<?=get_field('bg_cases_de_sucesso_2')['url']?>);">
			<h3 class="section-title"><?php the_field('titulo_cases_sucesso') ?></h3>
			<img src="<?=get_field('imagem_cases_de_sucesso_2')['url']?>">
			<h1><?php the_field('descrição_cases_de_sucesso_2') ?></h1>
			<p><br/><a class="btn btn-default btn-open-white" href="<?=get_field('link_cases_de_sucesso_2')?>" target="_blank" role="button">+SAIBA MAIS</a></p>
		</div>
		<div class="sb-cases-item" style="min-height: 300px; background-image: url(<?=get_field('bg_cases_de_sucesso_3')['url']?>);">
			<h3 class="section-title"><?php the_field('titulo_cases_sucesso') ?></h3>
			<img src="<?=get_field('imagem_cases_de_sucesso_3')['url']?>">
			<h1><?php the_field('descrição_cases_de_sucesso_3') ?></h1>
			<p><br/><a class="btn btn-default btn-open-white" href="<?=get_field('link_cases_de_sucesso_3')?>" target="_blank" role="button">+SAIBA MAIS</a></p>
		</div>
		<div class="sb-cases-item" style="min-height: 300px; background-image: url(<?=get_field('bg_cases_de_sucesso_4')['url']?>);">
			<h3 class="section-title"><?php the_field('titulo_cases_sucesso') ?></h3>
			<img src="<?=get_field('imagem_cases_de_sucesso_4')['url']?>">
			<h1><?php the_field('descrição_cases_de_sucesso_4') ?></h1>
			<p><br/><a class="btn btn-default btn-open-white" href="<?=get_field('link_cases_de_sucesso_4')?>" target="_blank" role="button">+SAIBA MAIS</a></p>
		</div>
	</div>
</section>


<section class="sb-marcas-2 hide-xs" id="marcas2">
	<div class="row">
		<div class="col-sm-8 mx-auto" style="padding: 22px;">
			<img src= "<?=get_field('imagens_faixa_noticias')['url']?>">
		</div>
	</div>
</section>



<section class="sb-quebracabeca text-center" id="quebracabeca">
	<h3 class="section-title title-orange light-orange" style="margin: 10px 0px 0px 0px;"><?php the_field('titulo_seção_propaganda') ?></h3>
	<div class="row">
		<div class="col-sm-4 mx-auto">
			<h2 class="simple"><?php the_field('subtitulo_seção_propaganda') ?></h2>
			<div class="text-center light-orange"><?php the_field('descrição_seção_propaganda') ?></div>
			<a class="btn btn-default btn-filled-white" href="#fale" role="button">FALE CONOSCO</a>
		</div>
	</div>
</section>

<!-- <section class="sb-fale text-center" id="fale">
	<h3 class="section-title title-dark">Assine nossa newsletter e vire um expert em marketing</h3>
	<div class="row mx-auto text-center">
		<div class="text-center mx-auto row-sm-6">
			<input class="news-name bluegray-input" type="text" placeholder="Primeiro nome...">
			<input class="news-mail bluegray-input" type="mail" placeholder="Email..." name="nae">
			<a class="news-send btn btn-default btn-filled-blue smaller" href="#" role="button">ASSINAR</a>
		</div>
	</div>
</section> -->

<section class="sb-blog text-center hide-xs" id="faleBlog">
	<h3 class="section-title title-dark"><?php the_field('titulo_blog') ?></h3>
	<div class="row col-sm-10 mx-auto text-center">
		<?php 
		   // the query
		   $the_query = new WP_Query( array(
		      'posts_per_page' => 3,
		   ));
		?>

		<?php if ( $the_query->have_posts() ) : ?>
		  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

			<div class="col-sm-3 mx-auto text-center">
				<article class="blog-1">
					<div class="blog-image" style="background-image: url(<?=the_post_thumbnail_url()?>)"></div>
					<div class="blog-category"><?=get_the_category()[0]->cat_name?></div>
					<div class="blog-text text-left"><?php the_title(); ?></div>
					<div class="blog-button text-right">
						<a href="<?=get_post()->guid?>"><button class="btn btn-default btn-round-open">Ver evento</button></a>
					</div>
				</article>
			</div>
		    
		    <?php //the_excerpt(); ?>

		  <?php endwhile; ?>
		  <?php wp_reset_postdata(); ?>

		<?php else : ?>
		  <p><?php __('Sem posts'); ?></p>
		<?php endif; ?>
<!-- 		<div class="col-sm-3 mx-auto text-center">
			<article class="blog-1">
				<div class="blog-image"></div>
				<div class="blog-category">Artigo</div>
				<div class="blog-text text-left">Overlays: melhorando a retenção, reengajamento e conversão do seu site</div>
				<div class="blog-button text-right"><button class="btn btn-default btn-round-open">Ver evento</button></div>
			</article>
		</div>
		<div class="col-sm-3 mx-auto text-center">
			<article class="blog-2">
				<div class="blog-image"></div>
				<div class="blog-category">Artigo</div>
				<div class="blog-text text-left">Links Patrocinados e E-mail Retargeting: Google AdWords + ShopTarget</div>
				<div class="blog-button text-right"><button class="btn btn-default btn-round-open">Ver evento</button></div>
			</article>
		</div>
		<div class="col-sm-3 mx-auto text-center">
			<article class="blog-3">
				<div class="blog-image"></div>
				<div class="blog-category">Artigo</div>
				<div class="blog-text text-left">Fidelizar clientes: Aprenda com exemplos do Esporte e da Música</div>
				<div class="blog-button text-right"><button class="btn btn-default btn-round-open">Ver evento</button></div>
			</article>
		</div> -->
	</div>
	<a class="btn btn-default btn-open-white" href="#" role="button">VEJA MAIS</a>
</section>


<section class="sb-news text-center hide-xs" id="faleNews">
	<h3 class="section-title title-dark"><?php the_field('titulo_noticias') ?></h3>
	<div class="row col-sm-10 mx-auto text-center">
		<div class="col-sm-3 mx-auto text-center">
			<article class="news-1">
				<div class="news-image" style="background-image: url(<?php the_field('imagem_news_1') ?>)"></div>
				<div class="news-category"><?php the_field('categoria_news_1') ?></div>
				<div class="news-text text-left"><?php the_field('descrição_news_1') ?></div> 
				<div class="news-button text-right"><a href="<?=get_field('link_botão_news_1')?>" target="_blank"><button class="btn btn-default btn-round-open">Ver evento</button></a></div>
			</article>
		</div>
		<div class="col-sm-3 mx-auto text-center">
			<article class="news-2">
				<div class="news-image" style="background-image: url(<?php the_field('imagem_news_2') ?>)"></div>
				<div class="news-category"><?php the_field('categoria_news_2') ?></div>
				<div class="news-text text-left"><?php the_field('descrição_news_2') ?></div>
				<div class="news-button text-right"><a href="<?=get_field('link_botão_news_2')?>" target="_blank"><button class="btn btn-default btn-round-open">Ver evento</button></a></div>
			</article>
		</div>
		<div class="col-sm-3 mx-auto text-center">
			<article class="news-3">
				<div class="news-image" style="background-image: url(<?php the_field('imagem_news_3') ?>)"></div>
				<div class="news-category"><?php the_field('categoria_news_3') ?></div>
				<div class="news-text text-left"><?php the_field('descrição_news_3') ?></div>
				<div class="news-button text-right"><a href="<?=get_field('link_botão_news_3')?>" target="_blank"><button class="btn btn-default btn-round-open">Ver evento</button></a></div>
			</article>
		</div>
	</div>
</section>


<section class="sb-comece" id="comece" style="background-image: url(<?=get_field('fund_secao_final')['url']?>)">
	<h3 class="section-title-orange"><?php the_field('titulo_seção_final') ?></h3>
	<div class="row">
		<div class="col-sm-7 mx-auto">
			<h1 class="white simple"><?php the_field('subtitulo_seção_final')?></h1>
			<br/>
			<a class="btn btn-default btn-filled-white btn-shadow" href="#" role="button">FALE CONOSCO</a>
		</div>
	</div>
</section>

<div class="wrapper" id="wrapper-index" style="display: none">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<main class="site-main" id="main">

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'loop-templates/content', get_post_format() );
						?>

					<?php endwhile; ?>

				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>

			</main><!-- #main -->

			<!-- The pagination component -->
			<?php understrap_pagination(); ?>

		</div><!-- #primary -->

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<script src="<?=get_template_directory_uri()?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri()?>/js/slick.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.sb-slick-content').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: false,
			asNavFor: '.sb-slick-sync'
		});
		$('.sb-slick-sync').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			asNavFor: '.sb-slick-content',
			dots: false,
			centerMode: false,
			arrows: false,
			focusOnSelect: true,
			infinite: true
		});
		$('.sb-cases-list').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  dots: true,
		  arrows: false
		});
		$('.sb-marcas-list').slick({
			slidesToShow: 7,
			slidesToScroll: 7,
			asNavFor: '.sb-slick-sync'
		});
		$('.sb-produtos-item').on('click',function(){
			$.each($('.sb-produtos-item').not('.slick-current'),function(){
				$(this).css('backgroundImage',$(this).data('imgactive'))
			})
			$('.sb-produtos-item.slick-current').css('backgroundImage',$(this).data('imginactive'))
		})

});
</script>


<style type="text/css">
	.home-bg{
		background: url(<?=get_field('home_bg')['url']?>);
	}
.sb-produtos-item.item1{ background-image: url(<?=get_field('(home)_prod_imagem_ícone_laranja_1')['url']?>) }
.sb-produtos-item.item1:hover, .sb-produtos-item.item1.slick-current{ background-image: url(<?=get_field('imagem_icone_produtos_1')['url']?>) }
.sb-produtos-item.item2{ background-image: url(<?=get_field('(home)_prod_imagem_ícone_laranja_2')['url']?>) }
.sb-produtos-item.item2:hover, .sb-produtos-item.item2.slick-current{ background-image: url(<?=get_field('imagem_icone_produtos_2')['url']?>) }
.sb-produtos-item.item3{ background-image:url(<?=get_field('(home)_prod_imagem_ícone_laranja_3')['url']?>) }
.sb-produtos-item.item3:hover, .sb-produtos-item.item3.slick-current{ background-image:  url(<?=get_field('imagem_icone_produtos_3')['url']?>) }
.sb-produtos-item.item4{ background-image: url(<?=get_field('(home)_prod_imagem_ícone_laranja_4')['url']?>) }
.sb-produtos-item.item4:hover, .sb-produtos-item.item4.slick-current{ background-image: url(<?=get_field('imagem_icone_produtos_4')['url']?>) }
.sb-produtos-item.item5{ background-image: url(<?=get_field('(home)_prod_imagem_ícone_laranja_5')['url']?>) }
.sb-produtos-item.item5:hover, .sb-produtos-item.item5.slick-current{ background-image:  url(<?=get_field('imagem_icone_produtos_5')['url']?>) }

</style>
<?php get_footer(); ?>
