<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link href='https://fonts.googleapis.com/css?family=Exo:100,200,300,400,600,700,800,900' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick-theme.css">
	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/shopback.css">
	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/blur.css"/>
	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/slicknav.min.css"/>
	<?php if (get_the_ID() == '297'): ?>
	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/login.css"/>
	<script src="<?=get_template_directory_uri()?>/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?=get_template_directory_uri()?>/js/slick.min.js"></script>

	<?php endif; ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header class="header-blog">
		<div class="progress-bar" id="leitura"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-3 text-center">
						<a href="<?php echo get_site_url(); ?>/">

							<img id="logo-gray" class="displaynone" src="<?=get_template_directory_uri()?>/img/shopback-logo-gray.png">
							<img id="logo-white" src="<?=get_template_directory_uri()?>/img/shopback-logo-white.png">

						</a>
					</div>
					<div class="col-md-6 text-center">
						<ul class="menu-header">
							<a href="<?php echo get_site_url(); ?>/quem-somos"><li class="item-menu-header <?php if (get_the_ID() == '286'): ?> item-header-active <?php endif; ?>">Quem somos</li></a>
							<a href="<?php echo get_site_url(); ?>/quem-somos/produtos"><li class="item-menu-header <?php if (get_the_ID() == '4'): ?> item-header-active <?php endif; ?>">Produtos</li></a>
							<a href="<?php echo get_site_url(); ?>#clientes"><li class="item-menu-header <?php if (get_the_ID() == '311'): ?> item-header-active <?php endif; ?>">Clientes</li></a>
							<a href="<?php echo get_site_url(); ?>/news"><li class="item-menu-header <?php if (get_the_ID() == '302'): ?> item-header-active <?php endif; ?>">News</li></a>
							<a href="<?php echo get_site_url(); ?>/blog"><li class="item-menu-header">Blog</li></a>
						</ul>
					</div>
					<div class="col-md-3 login text-center">
						<a class="item-menu-header" href="<?php echo get_site_url(); ?>/login">Login</a>
					</div>
				</div>
			</div>
		<span class="clear"></span>
	</header>

	<div class="hfeed site sb-theme <?=is_page_template('home.php')?'home-bg':'sb-page'?>" id="page">

		<!-- ******************* The Navbar Area ******************* -->
		<div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar">

			<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content',
			'understrap' ); ?></a>


			</div><!-- .wrapper-navbar end -->
