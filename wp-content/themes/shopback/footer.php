<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_sidebar( 'footerfull' ); ?>


<div class="wrapper" id="wrapper-footer" <?=is_page()?'style="margin-bottom: -30px;"':''?>>

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">
				<footer class="site-footer" id="colophon">

					<div class="site-info">
						<div class="row">
							<div class="col-sm-9 mx-auto">
								<ul class="navbar-right sb-menu navbar-bottom">
									<li class="sb-menu-item"><a href="#quemsomos">Quem somos</a></li>
									<li class="sb-menu-item"><a href="#produtos">Produtos</a></li>
									<li class="sb-menu-item"><a href="#clientes">Clientes</a></li>
									<li class="sb-menu-item"><a href="#news">News</a></li>
									<li class="sb-menu-item"><a href="#contratando">Blog</a></li>
									<li class="sb-menu-item"><a href="#login">Fale conosco</a></li>
								</ul>
								<div class="footer-address">
									<p><?php the_field('endereço') ?></p>
									<p><?php the_field('telefone') ?></p>
								</div>
								<div class="footer-social">
									<a href="<?=get_field('link_twitter')?>" target="_blank"><span class="footer-twitter"></span></a>
									<a href="<?=get_field('link_linkedin')?>" target="_blank"><span class="footer-linkedin"></span></a>
									<a href="<?=get_field('link_facebook')?>" target="_blank"><span class="footer-facebook"></span></a>
									<a href="<?=get_field('link_instagram')?>" target="_blank"><span class="footer-instagram"></span></a>
								</div>
								<div class="footer-logo">
									<img class="footer-logo" src="<?=get_template_directory_uri()?>/img/src/shopback-white-logo.png">
								</div>
							</div>
							<div class="col-sm-3 mx-auto">
								<div class="form-group">
									<p class="footer-ns-assine">Assine nossa newsletter</p>
									<input type="text" class="form-control gray-filled-input" id="newsletter-name" placeholder="Primeiro nome...">
								</div>
								<div class="form-group">
									<input type="email" class="form-control gray-filled-input" id="newsletter-email" placeholder="Email">
								</div>
								<button type="submit" id="newsletter-send" class="btn btn-full btn-filled-orange">ASSINAR</button>
							</div>
						</div>
					</div><!-- .site-info -->
					<div class="footer-line text-center">ShopBack © Todos os direitos reservados.</div>
				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->
<script src="<?=get_template_directory_uri()?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri()?>/js/slick.min.js"></script>
<script src="<?=get_template_directory_uri()?>/js/slicknav.min.js" type="text/javascript"></script>
<script src="<?=get_template_directory_uri()?>/js/jquery.slicknav.min.js" type="text/javascript"></script>
<script src="<?=get_template_directory_uri()?>/js/customizer.js" type="text/javascript"></script>
<script src="<?=get_template_directory_uri()?>/js/customizer.js" type="text/javascript"></script>
<?php wp_footer(); ?>

</body>

</html>
