<?php 
//	Template Name: Login


get_header();


?>


<div class="wrapper quem-somos" id="page-wrapper">
	<section id="logoin">
		<div class="container text-center">
			<a href="<?php echo get_site_url(); ?>/"><img src="<?=get_template_directory_uri()?>/img/shopback-logo-gray.png"></a>
		</div>
	</section>
	<section id="login">
		<div class="container">
			<div class="text-center">
				<div class="box-login">
					<form action="" method="">
						<div class="form-control">
							<label for="email" class="email">Email</label>
							<input class="form-group" id="email" type="text" name="email">
						</div>
						<div class="form-control">
							<label for="senha" class="email">Senha<a class="esqueci pull-right">Esqueci a senha</a></label>
							
							<input class="form-group" id="senha" type="password" name="senha">
						</div>
						<div class="form-check text-left">
							<label class="form-check-label">
								<input type="checkbox" class="custom-check">
						    	Mantenha-me conectado
							</label>
						</div>
						  <button type="submit" class="btn btn-default btn-filled-orange">ENTRAR</button>
					</form>
					<h3>Não tem conta?<a href="#"> Cadastre-se agora</a></h3>
				</div>
			</div>
		</div>
	</section>

</div><!-- Container end -->


<?php get_footer(); ?>

