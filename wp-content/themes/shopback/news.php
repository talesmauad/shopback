<?php /* Template Name: News */ ?>
<?php
wp_enqueue_style('shopback-news', get_bloginfo('template_url').'/css/news.css', '1.0.0');
get_header();
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>
<?php $posttype = get_post_type_object('the_news'); ?>
 	<?php $args = array('post_type'=>$posttype->name, 'posts_per_page'=>1, 'meta_query' => array(
 		array('key' => 'destaque', 'value' => true),
 	)); ?>
 	<?php $query = new WP_Query($args); ?>
 	<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>

<div id="primary" class="content-area news-featured" style="padding-top:150px;">
	<main id="main" class="site-main" role="main">
		<div class="container-fluid">
			<div class="row row-center">
				<div class="col-sm-10 col-md-9 col-lg-5 col-xs-10 text-center">
					<p class="post-meta">Em destaque | <?php echo get_field('data'); ?> | <?php echo get_field('fonte'); ?></p>
					<h2 class="post-title"><?php the_title(); ?></h2>
					<a class="btn btn-default btn-filled-orange" href="<?php echo get_field('link'); ?>" role="button">LER NOTÍCIA</a><br/>
				</div>
			</div>
		</div>
	</main><!-- .site-main -->
</div><!-- .content-area -->
<?php endwhile; endif; ?>

<section class="sb-news-page">
	<h3 class="section-title arrow-down">Outras notícias</h3>
	<div class="container-fluid">
		<div class="row row-center">
			<div class="col-xs-10 col-sm-12 col-md-10">
				<div class="row row-center">
					<?php $posttype = get_post_type_object('the_news'); ?>
					 	<?php $args = array('post_type'=>$posttype->name, 'posts_per_page'=>10,); ?>
					 	<?php $query = new WP_Query($args); ?>
					 	<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
						
								
								<div class="news-card
									<?php if(get_field('destaque') == true): ?>
										 big
									<?php endif; ?>

		
								 ">
									<div class="post-meta">Em destaque | <?php echo get_field('data'); ?> | <?php echo get_field('fonte'); ?></div>
									<h2 class="post-title"><?php the_title(); ?></h2>
									<div class="post-text">
										<?php the_content(); ?>
									</div>
									<div class="post-button">
										<a class="btn btn-default btn-filled-orange" href="<?php echo get_field('link'); ?>" role="button">LER NOTÍCIA</a><br/>
									</div>
								</div>
						<?php endwhile; endif; ?>
							
				</div>
			</div>
		</div>
	</div>
</section>


<?php get_footer(); ?>
