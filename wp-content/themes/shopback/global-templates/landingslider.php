<?php
/**
 * Slider setup
 *
 * @package understrap
 */
//get_theme_mod('shopback_slanding_cta')
?>

<?php if ( is_active_widget( false, 'slanding' ) ) : ?>


<div class="wrapper" id="wrapper-slanding">

<nav class="navbar navbar-fixed-top navbar-home">
	<div class="container">
		<div class="flag hidden-xs">
			<a href="http://www.shopback.com.br">PT</a> / <a href="http://www.shopback.co">EN</a> / <a href="http://www.shopback.co/es">ES</a>
		</div>

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<div class="flag visible-xs-block">
				<a href="http://www.shopback.com.br">PT</a> / <a href="http://www.shopback.co">EN</a> / <a href="http://www.shopback.co/es">ES</a>
			</div>

			<a class="navbar-brand" href="index.html">
				<img class="standard-logo" src="assets/img/shopback-logo.png" alt="ShopBack">
				<img class="white-logo" src="assets/img/shopback-white-logo.png" alt="ShopBack">
			</a>
		</div>
	</div>
</nav>

</div>


<?php endif; ?>
