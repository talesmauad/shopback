<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'shopback-db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'H@ckyng7');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l ~Ok@Q,f2Z~I^:V9PPEQdi$$-2R-ji>nCtwyf<X^g.}q|<EJ@kw7+uaFy%XV]-+');
define('SECURE_AUTH_KEY',  '|qMhD-{j?Rp5r+xf]*!apPLDFu~Viq^GhpOkW*6=i%KZXNsdJ+D/ab}|_vF`O0-w');
define('LOGGED_IN_KEY',    '@uN}gA8DbHPp%%Ay7Qb 4 TLI$X1B!gJV^|Tnp4;sv$ro,57P,!nv )vGC|F)NO>');
define('NONCE_KEY',        '2++(3/Vk(?f56|kuUrJC-hJ|l<@H<Y|FE.Se>JL=62@8uFq}OX}4h6k)l&YWVD&)');
define('AUTH_SALT',        'X1-#8.H6Oc*^LGj._^?=9{^Wv/)B!~&QA9-8my=d7m=s<1{PZ(A$;}SV9G-]b/A{');
define('SECURE_AUTH_SALT', 'rFjkmDflCXiPxK}7aGi)&rTL1t{t*#,9 0]Nkmy)yFTz&fy3Dd/ICk?xyXfJ</y!');
define('LOGGED_IN_SALT',   'Q@#@iWS1]szNj6X+P*,3+-Y-CX?-D%:2z(h^2e/_sGPgP5~~NEjtj|+{+M+I+aOB');
define('NONCE_SALT',       'nu[h5Uw/r$7Q-s.@:BgZ+j|zaxQD4?vK_S&*WMo)26Nd CvTU<>+`kCS*TP[H~K.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
