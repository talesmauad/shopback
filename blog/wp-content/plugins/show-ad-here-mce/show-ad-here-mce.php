<?php
/*
 Plugin Name: Show Ad Here (TinyMCE)
 Plugin URI: http://bossabox.com
 Description: Plugin to show ad on bossabox blog
 Version: 1.0
 Author: Eduardo Koller
 Author URI: http://bossabox.com
 License: GPL2
 */
function enqueue_plugin_scripts($plugin_array) {
    //enqueue TinyMCE plugin script with its ID.
    $plugin_array["insert_ad_button_plugin"] =  plugin_dir_url(__FILE__) . "index.js?12";
    return $plugin_array;
}

add_filter("mce_external_plugins", "enqueue_plugin_scripts");

function register_buttons_editor($buttons){
    //register buttons with their id.
    array_push($buttons, "insert_ad_button");
    return $buttons;
}

add_filter("mce_buttons", "register_buttons_editor");
