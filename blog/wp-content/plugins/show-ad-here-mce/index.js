(function() {
    tinymce.create("tinymce.plugins.insert_ad_button_plugin", {

        //url argument holds the absolute url of our plugin directory
        init : function(ed, url) {
          var state;
          console.log(ed);
          jQuery(window).load(function(){
            if(ed.dom.$('#see-also-container').length) {
              state = true;
              ed.fire('insert_ad', {state: state});
            }
          });
          jQuery('#acf-anuncio').hide();

            //add new button
            ed.addButton("insert_ad_button", {
                title : "Inserir Anúncio",
                cmd : "insert_ad_command",
                image : "https://cdn4.iconfinder.com/data/icons/gradient-ui-1/512/plus-32.png",
                onPostRender: function(){
                    var self = this;
                    ed.on('insert_ad', function(e) {
                        self.active(e.state);
                    });
                }
            });

            //button functionality.
            ed.addCommand("insert_ad_command", function() {
                state = !state;
                ed.fire('insert_ad', {state: state});
                if (state){
                  var return_text = "<div id='see-also-container' class='see-also-container'><h4>Anúncio</h4></div>";
                  ed.execCommand("mceInsertContent", 0, return_text);
                  jQuery('#acf-field-anuncio-1').click();
                  alert('Não se esqueça de selecionar os posts que poderão aparecer como anúncio no fim da página!');
                } else {
                  ed.dom.$('#see-also-container').remove();
                  jQuery('#acf-field-anuncio-1').click();
                }
            });

        },

        createControl : function(n, cm) {
            return null;
        },

        getInfo : function() {
            return {
                longname : "Insert Ad Button",
                author : "Eduardo Koller",
                version : "1"
            };
        }
    });

    tinymce.PluginManager.add("insert_ad_button_plugin", tinymce.plugins.insert_ad_button_plugin);
})();
