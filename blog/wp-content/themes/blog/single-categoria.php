<?php
/*
Template Name: Single Categoria
*/
 get_header(); ?>
 	<?php $args = array('post_type'=>array('post'),

                        'posts_per_page'=>1,
                        'meta_query'    => array(
                            'relation'      => 'AND',
                            array(
                                'key'       => 'destaque',
                                'value'     => true,
                            ),
                            array(
                                'key'       => 'categoria',
                                'value'     => 'Tecnologia',
                                'compare'   => 'post-title'
                            ),
                        ),
                    );
                    ?>
 	<?php $query = new WP_Query($args); ?>
 	<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
		<section id="featured-post" style="background: url(<?php the_post_thumbnail_url('full'); ?>) center center; background-size: cover; background-attachment: fixed; background-repeat: no-repeat; background-position-y: 100%;">
			<div class="vertical-align">
				<div class="container">
					<div class="row text-center">
						<div class="col-lg-6 col-md-6 col-md-offset-3 col-lg-offset-3 col-sm-offset-3 col-sm-6 post">
                            <span class="leitura">Em destaque</span>
							<a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a>
							<div class="meta text-center">
								<a href="<?php the_permalink(); ?>" class="leia-mais inverted">Ler artigo</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
 	<?php endwhile; endif; ?>
	<section id="posts">
		<div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="filter dropdown">
                        <button class="dropdown-toggle" type="button" data-toggle="dropdown">Eu me interesso por
                        <b>
                        <?php 
                            $categoria = get_field('categoria');
                            echo $categoria->post_title;
                        ?>                                    
                        </b> <span class="caret"></span></button>
                        <ul class="dropdown-menu">

                            <li><a href="<?php bloginfo('url'); ?>/">Eu me interesso por <b>tudo</b></a></li>
                            <?php
                                $args = array(
                                    'post_type' => 'categoria',
                                    'posts_per_page'=> -1,
                                    'orderby'  => array( 
                                            'ID' => 'DESC' ,
                                        ),
                                );
                                $query = new WP_Query( $args );
                                $query_contents=Array();
                                while ( $query->have_posts() ) {
                                    $query->the_post();
                                    array_push($query_contents,Array(
                                        "id"=>get_the_ID(),
                                        "title"=>get_the_title(),
                                        "url"=>get_permalink(),
                                    ));?>
                                    <li><a href="<?php bloginfo('url'); ?>/<?php the_title(); ?>">Eu me interesso por <b><?php the_title() ?></b></a></li>
                                    
                            <?php } ?>
        

<!--                             <li><a href="<?php bloginfo('url'); ?>/negocios">Eu me interesso por <b>negócios</b></a></li>
                            <li><a href="<?php bloginfo('url'); ?>/marketing">Eu me interesso por <b>marketing</b></a></li>
                            <li><a href="<?php bloginfo('url'); ?>/produtos">Eu me interesso por <b>produtos</b></a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="posts">
                <div class="row sm-flex sm-flex-wrap">
					<?php if($_GET['s']): ?>
						<div class="col-xs-12">
							<h2 class="text-center">Busca: <?php echo $_GET['s']; ?></h2>
						</div>
					<?php endif; ?>
                    <?php $count_lg = 0; ?>
                    <?php $count_md = 0; ?>
                    <?php $curr_lg_order = 1; ?>
                    <?php $curr_md_order = 1; ?>
                    <?php $last_lg_order = 0; ?>
                    <?php $last_md_order = 0; ?>
					<?php $pts = array('post' => '', 'pocketbook' => 'Pocket Book', 'especial' => 'Especial'); ?>
					<?php $paged = get_query_var('page') ? get_query_var('page') : 1; ?>
					<?php $args = array('post_type'=>array('post','especial','pocketbook'), 'posts_per_page'=>12, 'paged' => $paged); ?>
 					<?php if($_GET['s']) $args['s'] = $_GET['s']; ?>
					<?php $query = new WP_Query($args); ?>
					<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
                        <?php $pt = get_post_type(); ?>
                        <?php if($count_lg!= 0 && $count_lg%3==0) $curr_lg_order += 2; ?>
                        <?php if($count_md!=0 && $count_md%2==0) $curr_md_order += 2; ?>
                        <?php if($pt=='especial' || $pt=='pocketbook'): ?>
                            <?php $last_lg_order = $curr_lg_order+1; ?>
                            <?php $last_md_order = $curr_md_order+1; ?>
                            <li class="card post full <?= 'lg-order-'.$last_lg_order ?> <?= 'md-order-'.$last_md_order ?>">
                                <div class="img-wrapper" style="background: url(<?php the_post_thumbnail_url('post-thumbnail-big');?>) center center; background-size: cover;">
                                    <?php the_post_thumbnail('post-thumbnail', array('class' => 'img-responsive hidden-md hidden-lg hidden-sm')); ?>
                                </div>
                                <div class="full-wrapper">
                                    <div class="text-wrapper">
                                        <p class="categories"><?php echo 'Artigo' ?> | <?php
                                            $categoria = get_field('categoria');
                                            echo $categoria->post_title;

                                         ?></p>
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <div class="bottom bottom-especial">
                                        <p><?php the_field('tempo_de_leitura'); ?> de leitura</p>
                                        <a href="<?php the_permalink(); ?>" class="read-more">Ler artigo</a>
                                    </div>
                                </div>
                            </li>
                            <?php $count_lg += 3; ?>
                            <?php $count_md += 2; ?>
                        <?php else: ?>
                            <li class="card post <?= 'lg-order-'.$curr_lg_order ?> <?= 'md-order-'.$curr_md_order ?>">
                                <div class="img-wrapper">
                                    <?php the_post_thumbnail('post-thumbnail', array('class' => 'img-responsive')); ?>
                                </div>
                                <div class="text-wrapper">
                                    <p class="categories">Artigo | <?php
                                        $categoria = get_field('categoria');
                                        echo $categoria->post_title;

                                         ?></p>
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                </div>
                                <div class="bottom">
                                    <p><?php the_field('tempo_de_leitura'); ?> de leitura</p>
                                    <a href="<?php the_permalink(); ?>" class="read-more">Ler artigo</a>
                                </div>
                            </li>
                            <?php $count_lg++; ?>
                            <?php $count_md++; ?>
                        <?php endif; ?>
					<?php endwhile; ?>
					<?php else : ?>
						<div class="col-xs-12">
							<p class="text-center">Desculpe, nenhum resultado encontrado.</p>
						</div>
					<?php endif; ?>
                    <?php if($count_lg%3!=0 || $count_md%2!=0) :
                        $next_lg = 3*ceil($count_lg/3);
                        $need_lg = $next_lg - $count_lg;
                        $next_md = 2*ceil($count_md/2);
                        $need_md = $next_md - $count_md;
                        $fill = 0;
                        ?>
                        <?php $paged = get_query_var('page') ? get_query_var('page') : 1; ?>
                        <?php $paged++; ?>
                        <?php $args = array('post_type'=>'post', 'posts_per_page'=>12, 'paged' => $paged); ?>
                        <?php if($_GET['s']) $args['s'] = $_GET['s']; ?>
                        <?php $query_need = new WP_Query($args); ?>
                        <?php if($query_need->have_posts()) : while($query_need->have_posts()) : $query_need->the_post(); ?>
                            <?php $class = ''; ?>
                            <?php if($fill>=$need_lg) $class .= ' hidden-lg'; ?>
                            <?php if($fill>=$need_md) $class .= ' hidden-xs hidden-sm'; ?>
                            <li class="card post <?= 'lg-order-'.$curr_lg_order ?> <?= 'md-order-'.$curr_md_order ?> <?= $class ?>">
                                <div class="img-wrapper">
                                    <?php the_post_thumbnail('post-thumbnail', array('class' => 'img-responsive')); ?>
                                </div>
                                <div class="text-wrapper">
                                    <p class="categories"><?php
                                        $categoria = get_field('categoria');
                                        echo $categoria->post_title;

                                         ?></p>
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                </div>
                                <div class="bottom">
                                    <p><?php the_field('tempo_de_leitura'); ?> de leitura</p>
                                    <a href="<?php the_permalink(); ?>" class="read-more">Ler artigo</a>
                                </div>
                            </li>
                            <?php $fill++; if($fill>=$need_lg && $fill>=$need_md) break;  ?>
                        <?php endwhile; endif; ?>
                    <?php endif; ?>
				    <span class="clear"></span>
			    </div>
            </ul>
		</div>
        <div class="pagenavi col-xs-12 text-center">
            <?php wp_pagenavi(array('query' => $query)); ?>
        </div>
	</section>
    <style>
        @media (min-width: 768px) and (max-width: 1023px) {
            <?php $i = 1; while($i<=$count_md): ?>
                .md-order-<?= $i ?> {
                    order: <?= $i ?>
                }
                <?php $i++; ?>
            <?php endwhile; ?>
        }
        @media (min-width: 1024px) {
            <?php $i = 1; while($i<=$count_lg): ?>
                .lg-order-<?= $i ?> {
                    order: <?= $i ?>
                }
                <?php $i++; ?>
            <?php endwhile; ?>
        }
    </style>
<?php get_footer(); ?>
