<?php

wp_enqueue_style('slick-css', get_bloginfo('template_url').'/css/slick.css', array(), '1.0.0'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php if(is_search()) : echo 'Busca'; elseif($titulo = get_queried_object()->labels->name): echo $titulo; else: the_title(); endif; ?> | bossabox</title>
	<?php if(is_single()): ?>
		<meta property="og:title" content="<?php the_title(); ?>">
		<meta property="og:type" content="article">
		<meta property="og:url" content="<?php the_permalink(); ?>">
		<meta property="og:site_name" content="bossabox">
		<meta property="og:image" content="<?php the_post_thumbnail_url('full'); ?>">
		<?php $tn_id = get_post_thumbnail_id( $post->ID );
		$img = wp_get_attachment_image_src( $tn_id, 'full' );
		$width = $img[1];
		$height = $img[2]; ?>
		<meta property="og:image:width" content="<?php echo $width; ?>">
		<meta property="og:image:height" content="<?php echo $height; ?>">
		<meta property="og:description" content="<?php echo get_the_excerpt(); ?>">
	<?php else : ?>
		<meta property="og:title" content="Blog">
		<meta property="og:type" content="website">
		<meta property="og:url" content="<?php the_permalink(); ?>">
		<meta property="og:site_name" content="bossabox">
		<meta property="og:image" content="http://bossabox.com/img/og-image.jpg">
		<meta property="og:image:width" content="1200">
		<meta property="og:image:height" content="613">
		<meta property="og:description" content="Escrevemos sobre empreendedorismo e tecnologia.">
	<?php endif; ?>
	<meta property="fb:app_id" content="1201442006555991">
    <?php wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css?family=Montserrat:200,300,400,400i,500,600,700,800|Source+Sans+Pro:300,400,600,700,900', array(), '1.0.0'); ?>
	<?php wp_enqueue_style('font-awesome', get_bloginfo('template_url').'/css/font-awesome.min.css', array(), '1.0.0'); ?>
	<?php wp_enqueue_style('icomoon', get_bloginfo('template_url').'/css/icomoon/style.css', array(), '1.0.1'); ?>
	<?php wp_enqueue_style('icomoon2', get_bloginfo('template_url').'/css/icomoon/style.css', array(), '1.0.1'); ?>
    <?php wp_enqueue_style('icomoon3', 'https://bossabox.com/css/icomoon/style3.css', array(), '1.0.0'); ?>
	<?php wp_enqueue_style('icomoon4', 'https://bossabox.com/css/icomoon/style4.css', array(), '1.0.0'); ?>
    <?php wp_enqueue_style('icomoon5', 'https://bossabox.com/css/icomoon/style5.css', array(), '1.0.0'); ?>
    <?php wp_enqueue_style('icomoon6', 'https://bossabox.com/css/icomoon/style6.css', array(), '1.0.0'); ?>
    <?php wp_enqueue_style('icomoon7_2', 'https://bossabox.com/css/icomoon/style7.css', array(), '1.0.0'); ?>
    <?php wp_enqueue_style('icomoon7', get_bloginfo('template_url').'/css/icomoon/style7.css', array(), '1.0.0'); ?>
	<?php wp_enqueue_style('roboto-slab', 'https://fonts.googleapis.com/css?family=Roboto+Slab:400,700', array(), '1.0.0'); ?>
	<?php wp_enqueue_style('bootstrap', get_bloginfo('template_url').'/css/bootstrap.min.css', array(), '1.0.0'); ?>
	<?php wp_enqueue_style('bootstrap-theme', get_bloginfo('template_url').'/css/bootstrap-theme.css', array('bootstrap'), '1.0.0'); ?>
	<?php wp_enqueue_style('select2', get_bloginfo('template_url').'/css/select2.css', array(), '1.0.0'); ?>
    <?php wp_enqueue_style('site-style', 'https://bossabox.com/style.css', array(), '1.0.1'); ?>
	<?php wp_enqueue_style('custom-style', get_bloginfo('template_url').'/style.css', array(), '1.0.75'); ?>

	<?php wp_head(); ?>
</head>
<body id="blog">
	<script>
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '1201442006555991',
	      xfbml      : true,
	      version    : 'v2.7'
	    });
	  };

	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "//connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-82380627-1', 'auto');
		ga('send', 'pageview');

	</script>
	<header>
		<nav role="navigation" class="navbar navbar-especiais">
			<span class="clear"></span>
		    <div class="container">
		    	<div class="row">
			        <!-- Collection of nav links and other content for toggling -->
			        <div id="navbarCollapse">
			            <ul class="nav navbar-nav">
			            	<li class="pull-right nav-close">
			            		<a href="http://bossabox.com/blog" title="Voltar para o blog" style="background: none !important; border: none !important;"><span class="icon7-fechar"></span></a>
			            	</li>
			            </ul>
			        </div>
		    	</div>
		    </div>
		</nav>
		<span class="clear"></span>
	</header>


	<?php $id = get_the_ID(); ?>
	<?php $args = array('post_type'=>'especial', 'posts_per_page'=>0, 'meta_query'=>array(array('key' => 'conjunto', 'value' => '"'.$id.'"', 'compare' => 'LIKE'))); ?>
	<?php $query = new WP_Query($args); ?>


	<div class="loader-wrapper">
		<div class="loader"></div>
	</div>
	<section id="especiais" style="background: url(<?php the_post_thumbnail_url('full'); ?>) center center; background-size: cover; background-repeat: no-repeat; background-position-y: 100%; background-color: #4b4265; background-blend-mode: multiply;">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 especiais-title">
					<p class="categories">Especial | <?php the_field('categoria'); ?></p>
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
			<div class="row">
					<ul class="especiais-carousel">
						<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
		                    <li>
		                    	<div class="card post">
			                        <div class="img-wrapper">
			                            <?php the_post_thumbnail('post-thumbnail', array('class' => 'img-responsive')); ?>
			                        </div>
			                        <div class="text-wrapper">
			                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			                        </div>
			                        <div class="bottom">
			                            <p><?php the_field('tempo_de_leitura'); ?> de leitura</p>
			                            <a href="<?php the_permalink(); ?>" class="read-more">Ler artigo</a>
			                        </div>
		                    	</div>
		                    </li>
		                <?php endwhile; endif; ?>
					</ul>
			</div>
		</div>
	</section>
<?php wp_enqueue_script('slick-js', get_bloginfo('template_url').'/js/slick.min.js', array('jquery'), '1.0.0'); ?>
<?php wp_enqueue_script('especiais-functions', get_bloginfo('template_url').'/js/especiais.js', array('jquery', 'slick-js'), '1.0.0'); ?>
<?php get_footer(); ?>
