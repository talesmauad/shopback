<?php 

/*
*	Template Name: Quem somos
*/

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );


?>
<?php get_template_part( 'global-templates/hero', 'none' ); ?>
<?php get_template_part( 'global-templates/landingslider', 'none' ); ?>


<div class="wrapper quem-somos" id="page-wrapper">

	<div class=" text-center" id="content" tabindex="-1">

		<div class="row">

			<main class="site-main" id="main">
				<section id="quem-somos-banner" class="text-center">
					<h1>Quem somos</h1>
					<p>Através de pesquisas, análises e intenso trabalho, desenvolvemos poderosos algoritmos que auxiliam na redução do abandono, aumento da conversão e otimização da experiência dos seus clientes.</p>
					<a class="btn btn-filled-orange" href="#fale" role="button">FALE CONOSCO</a>
				</section>
				<section id="texto-quem-somos">
					<p>Idealizada por sócios que respiram conversão desde os primórdios da internet la em 1995, a ShopBack teve o inicio de seu desenvolvimento em Outubro de 2013. Cansado das “mesmices” do mercado, a ideia era justamente lançar uma plataforma totalmente única e diferente do que todo o mercado já estava fazendo. Premissa básica: nossos clientes investem milhões em mídia por ano para trazer usuários ao site, porem nem 1/10 desse investimento é destinado a retenção desses mesmos potenciais usuarios que visitam o site e abandonam sem nem deixar rastros.</p>
					<p>Nascida oficialmente em Outubro de 2014, a ShopBack entrava no mercado como uma exclusiva multi-plataforma “SaaS” focada na retenção, re-engajamento e recaptura dos 98% dos usuários que abandonam seu site sem converter. E justamente após quase um ano de desenvolvimento, descobrimos que desenvolvemos um produto realmente excepcional, tão excepcional que era tão complexo para implementar que so bastava chamar o Bill Gates (ou nosso time de dev) que ele conseguiria! Exatamente, se você conhece alguma start-up que nasceu perfeita nos apresente esse fato inédito!!</p>
					<p>Realizando essa falha, decidimos refazer totalmente esse 1 ano de trabalho em 5 meses! Cargas horárias de trabalho dobradas, noites em claro, pizzas gordurosas na madrugada e por ai renascemos novamente em Fevereiro de 2015, voando como o Romario em 94!</p>
				</section>

			</main><!-- #main -->

		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
