<?php the_post(); get_header(); ?>

	<section id="featured-post" style="background: url(<?php the_post_thumbnail_url('full'); ?>) center center; background-size: cover; background-attachment: fixed; background-repeat: no-repeat; background-position-y: 100%;">
		<div class="vertical-align">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-md-offset-2 col-sm-6 post">
						<a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a>
						<div class="meta">
							<div class="author">
								<span><?php the_field('nome_autor'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="single">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<p><b><?php the_field('tempo_de_leitura'); ?> de leitura</b></p>

					<?php the_content(); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="5"></div>
				</div>
			</div>
		</div>

		<?php if(get_field('anuncio')): ?>
			<?php $posts = get_field('posts_para_anuncio'); ?>
			<?php $post = $posts[array_rand($posts)]; ?>
			<div class="see-also">
				<a href="<?php echo get_permalink($post); ?>">
					<div class="img-container"><?php echo get_the_post_thumbnail($post, 'full'); ?></div>
					<div class="text-container">
						<span class="leia-tambem">Leia também</span>
						<h4><?php echo get_the_title($post); ?></h4>
					</div>
				</a>
			</div>
		<?php endif; ?>
	</section>

	<?php wp_enqueue_script('see-also', get_bloginfo('template_url').'/js/see-also.js', array('jquery'), '1.0.2'); ?>
<?php get_footer(); ?>
