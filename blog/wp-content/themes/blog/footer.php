
    <meta name="vote-url" content="<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php">
    <?php if(!get_post_type()=='pocketbook'): ?>
        <section id="newsletter">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h2>Escrevemos sobre empreendedorismo e tecnologia</h2>
                        <form action="">
                            <input type="email" name="n_email" value="E-mail" onfocus="if(this.value=='E-mail'){this.value=''}" onblur="if(this.value==''){this.value='E-mail'}">
                            <input type="submit" value="Inscrever-se">
                        </form>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
    <?php wp_reset_query(); ?>
    <?php if(is_single() && !is_page() && get_post_type()!='pocketbook' && get_post_type()!='especiais'): ?>
        <meta name="post-id" content="<?php the_ID(); ?>">
        <nav class="navbar navbar-default navbar-fixed-bottom">
            <?php $vote = $_COOKIE['bbox-vote-'.get_the_ID()]; ?>
            <div class="container-fluid">
                <ul class="nav navbar-nav text-center">
                    <li class="hidden-sm hidden-xs">
                        <?php for($i = 1; $i<=5; $i++): ?>
                            <?php if($i==1): $frase = '<p>Avalie <b>1 ponto</b></p>'; else: $frase = '<p>Avalie <b>'.$i.' pontos</b></p>'; endif; ?>
                            <a class="star" href="#" data-vote="<?php echo $i; ?>" data-post-id="<?php echo get_the_ID(); ?>" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="<?php echo $frase; ?>"><i class="fa fa-star"></i></a>
                        <?php endfor; ?>
                    </li>
                    <li class="social">
                        <a href="mailto:?to=&body=<?php the_permalink(); ?>&subject=<?php the_title(); ?>:<?php the_permalink(); ?>' ,520,350);" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="<p>Compartilhe por <b>email</b></p>"><i class="fa fa-envelope-square"></i></a>
                        <a href="javascript:sharer('https://twitter.com/intent/tweet?source=<?php the_permalink(); ?>&text=<?php the_title(); ?>:<?php the_permalink(); ?>' ,520,350);" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="<p>Compartilhe no <b>Twitter</b></p>"><i class="fa fa-twitter-square"></i></a>
                        <a href="javascript:sharer('https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&summary=<?php echo get_the_excerpt(); ?>&source=<?php the_permalink(); ?>' ,520,350);" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="<p>Compartilhe no <b>LinkedIn</b></p>"><i class="fa fa-linkedin-square"></i></a>
                        <a href="javascript:sharer('https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>', 520, 350);" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="<p>Compartilhe no <b>Facebook</b></p>"><i class="fa fa-facebook-square"></i></a>
                        <a href="javascript:sharer('https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>', 520, 350);" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="<p>Compartilhe no <b>Google+</b></p>"><i class="fa fa-google-plus-square"></i></a>
                        <!-- <a class="hidden-sm hiddem-md hidden-lg" href="whatsapp://send?text=<?php the_permalink(); ?>" data-action="share/whatsapp/share" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="<p>Compartilhe no <b>Whatsapp</b></p>"><i class="icon2-whats"></i></a> -->
                    </li>
                    <li class="hidden-sm hidden-xs">
                        <a href="#comments" data-toggle="tooltip" data-placement="top" title="<p>Deixe um <b>comentário</b></p>"><i class="fa fa-comment"></i></a>
                    </li>
                    <?php $previous = get_previous_post(); ?>
                    <?php if($previous): ?>
                        <li class="hidden-sm hidden-xs pull-right">
                            <div class="next-post-tooltip tooltip fade top out">
                                <div class="tooltip-inner">
                                    <div class="wrapper">
                                        <img src="<?php echo get_the_post_thumbnail_url($previous->ID, 'post-thumbnail'); ?>" style="max-width:500px; height: auto;">
                                        <div class="next-post-text">
                                            <span>Próximo artigo</span>
                                            <p class="title"><?php echo get_the_title($previous->ID) ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo get_permalink($previous->ID); ?>" id="next-post"><i class="icon2-arrow"></i></a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </nav>
    <?php endif; ?>
<div class="wrapper" id="wrapper-footer" <?=is_page()?'style="margin-bottom: -30px;"':''?>>

    <div class="container">

        <div class="row">

            <div class="col-md-12">
                <footer class="site-footer" id="colophon">

                    <div class="site-info">
                        <div class="row">
                            <div class="col-sm-9 mx-auto">
                                <ul class="navbar-right sb-menu navbar-bottom">
                                    <li class="sb-menu-item"><a href="#quemsomos">Quem somos</a></li>
                                    <li class="sb-menu-item"><a href="#produtos">Produtos</a></li>
                                    <li class="sb-menu-item"><a href="#clientes">Clientes</a></li>
                                    <li class="sb-menu-item"><a href="#news">News</a></li>
                                    <li class="sb-menu-item"><a href="#contratando">Blog</a></li>
                                </ul>
                                <div class="footer-address">
                                    <p>Rua Fidencio Ramos, 195, cj. 71 - Vila Olímpia, São Paulo - SP</p>
                                    <p>Telefone: +55 11 2364-7974</p>
                                </div>
                                <div class="footer-social">
                                    <a href="<?=get_field('link_twitter')?>" target="_blank"><span class="footer-twitter"></span></a>
                                    <a href="<?=get_field('link_linkedin')?>" target="_blank"><span class="footer-linkedin"></span></a>
                                    <a href="<?=get_field('link_facebook')?>" target="_blank"><span class="footer-facebook"></span></a>
                                    <a href="<?=get_field('link_instagram')?>" target="_blank"><span class="footer-instagram"></span></a>
                                </div>
                                <div class="footer-logo">
                                    <img class="footer-logo" src="<?=get_template_directory_uri()?>/img/shopback-white-logo.png">
                                </div>
                            </div>
                            <div class="col-sm-3 mx-auto">
                                <div class="form-group">
                                    <input type="text" class="form-control gray-filled-input" id="newsletter-name" placeholder="Primeiro nome...">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control gray-filled-input" id="newsletter-email" placeholder="Email">
                                </div>
                                <button type="submit" id="newsletter-send" class="btn btn-full btn-filled-orange">ASSINAR</button>
                            </div>
                        </div>
                    </div><!-- .site-info -->
                    <div class="footer-line text-center">ShopBack © Todos os direitos reservados.</div>
                </footer><!-- #colophon -->

            </div><!--col end -->

        </div><!-- row end -->

    </div><!-- container end -->

</div><!-- wrapper end -->
    <script src="<?=get_template_directory_uri()?>/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="<?=get_template_directory_uri()?>/js/popper.min.js" type="text/javascript"></script>
    <script src="<?=get_template_directory_uri()?>/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?=get_template_directory_uri()?>/js/slicknav.min.js" type="text/javascript"></script>
    <script src="<?=get_template_directory_uri()?>/js/jquery.slicknav.min.js" type="text/javascript"></script>
    <script src="<?=get_template_directory_uri()?>/js/slick.min.js" type="text/javascript"></script>
    <script src="<?=get_template_directory_uri()?>/js/select2.min.js" type="text/javascript"></script>
    <script src="<?=get_template_directory_uri()?>/js/scripts.js" type="text/javascript"></script>
    <script src="<?=get_template_directory_uri()?>/js/validator.min.js" type="text/javascript"></script>
    <script src="<?=get_template_directory_uri()?>/js/functions.js" type="text/javascript"></script>
    <?php wp_footer(); ?>

    </div><!-- wrapper navbar -->
</body>
</html>
