$(document).ready(function(){
	$('.sb-slick-content').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: false,
	  asNavFor: '.sb-slick-sync'
	});
	$('.sb-slick-sync').slick({
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  asNavFor: '.sb-slick-content',
	  dots: false,
	  centerMode: false,
	  arrows: false,
	  focusOnSelect: true,
	  infinite: true
	});
	$('.sb-cases-list').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  dots: true,
	  arrows: true
	});
	$('.sb-produtos-item').on('click',function(){
		$.each($('.sb-produtos-item').not('.slick-current'),function(){
			$(this).css('backgroundImage',$(this).data('imgactive'));
		})
		$('.sb-produtos-item.slick-current').css('backgroundImage',$(this).data('imginactive'));
	});

});