jQuery(function($){
	var sHeight = window.innerHeight;
	var $imgBlur = jQuery('img.bg-blur');
	if(/Mobi/i.test(navigator.userAgent)){
		jQuery('.pocketbook-cover').height(sHeight);
		$imgBlur.css('opacity','1');
		var pHeight = jQuery('#pocketbook-featured').height();
		var iHeight = $imgBlur.height();
		if(pHeight>iHeight) {
			$imgBlur.height(pHeight);
		}
	}
});