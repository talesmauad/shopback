jQuery(function($){
	var loaded = false;
	var timeout = false;
	$(window).load(function(){
		loaded = true;
		if(timeout) {
			$('body').removeClass('loading').addClass('loaded');
		}
	});
	setTimeout(function(){
		timeout = true;
		if(loaded) {
			$('body').removeClass('loading').addClass('loaded');
		}
	}, 2000)
	$('ul.especiais-carousel').slick({
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 1023,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
});