jQuery(function($){
    // popup single
    var popped = false;
    var $single = $('#single');
    if($single.length){
        sh = $('#single').height()/2;
        offset = $('#single').offset().top;
    } else {
        sh = 0;
        offset = 0;
    }
    lastScroll = 0;

    var sub = getCookie('bbox-blog-newsletter');
    if(sub != 'subscribed' && sub != 'dontshow' && !mobile){
        if($('#single').get(0)) {
            var interval = setInterval(function(){
                var scroll = $(window).scrollTop();
                if(popped === false){
                    if(scroll>=(offset+sh)){
                        $('#popupSingle').modal();
                        popped = true;
                    }
                }
                lastScroll = scroll;
            },500);
        }
    }
    var subscribed = false;

    $('#subscribe-modal-form').submit(function(e){
        e.preventDefault();
        var ajaxUrl = $('meta[name=vote-url]').attr('content');
        if($('#modal-email').val()==''){
            alert('Por favor entre com um e-mail');
        } else {
            var uemail = $('#modal-email').val();
            var dataToSend = {action:'chimp_subscribe',email: uemail};
            subscribed = true;
            $('#popupSingle').modal('hide');
            alert('Obrigado por se inscrever!');
            $.ajax({
                type : "post",
                dataType : "json",
                url : ajaxUrl,
                data : dataToSend,
                success: function(response) {
                    console.log(response);
                }
            });
        }
    });

    $('#popupSingle').on('hidden.bs.modal', function () {

        if(subscribed){
            setCookie('bbox-blog-newsletter', 'subscribed', 999);
            console.log('SUBSCRIBED!');
        } else {
            setCookie('bbox-blog-newsletter', 'dontshow', 1);
            console.log('Closed modal, waiting a day to show again');
        }
    });
});