function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

jQuery(document).ready(function($){
	var mobile = false;

	if(/Mobi/i.test(navigator.userAgent)) {mobile = true;}

    //smooth scrolling
    $('a[href*=\\#]').on('click', function(event){
        event.preventDefault();
        var href = this.hash;
        var $target = $(this.hash);
        if($target.length){
            $('html,body').animate({scrollTop:$target.offset().top-50}, 500, function(){
                window.location.hash = href;
            });
        }
    });

    if($('section#single').length) {
        $('.progress-bar#leitura').show();
        $('.progress-bar#leitura').css('opacity', '1');
        jQuery(window).scroll(function() {
            var wheight = $(window).height();
            var width = $(window).width();
            var scrollTop = $(window).scrollTop();
            var totalHeight = $('section#single').height();
            var offset = $('section#single').offset().top;
            var percentage = (scrollTop+wheight-offset)/(totalHeight);
            var newWidth = width * percentage;
            $('.progress-bar#leitura').width(newWidth);
        });
    }

    var windowWidth = $(window).width();
    var windowHeight = $(window).height();
    if(windowWidth<1023) {
        jQuery('nav.navbar-default').removeAttr('data-offset-top');
    }
    if(windowWidth<768) {
        jQuery('section#featured-post').height(windowHeight-50);
    }

    if(windowWidth<=1024) {
        $('.sharer').click(function(e) {
            e.preventDefault();
            if($('div.share').hasClass('active')) {
                $('div.share').removeClass('active');
            } else {
                $('div.share').addClass('active');
            }
        });
    } else {
        $('div.share').hover(function() {
            $('div.share').addClass('active');
        }, function() {
            $('div.share').removeClass('active');
        });
    }

    $("#single a[href^='#']").on('click', function(e) {

       // prevent default anchor click behavior
       e.preventDefault();

       // store hash
       var hash = this.hash;
       console.log(hash);

       // animate
       $('html, body').animate({
           scrollTop: $(hash).offset().top-80
         }, 800, function(){

           // when done, add hash to url
           // (default click behaviour)
           window.location.hash = hash;
         });

    });

    if($('#single').length){
        var nextP = $('#next-post');

        var img = new Image();
        img.src = nextP.attr('data-load-before');
        img.onload = function(){
            console.log(img);
        }

           if(window.innerWidth > 1024){
            $("body").tooltip({ selector: '[data-toggle=tooltip]', html: true, container: 'body', trigger: 'hover' });
            $('#next-post').hover(function(){
                $('.next-post-tooltip').removeClass('out');
                $('.next-post-tooltip').addClass('in');
                $('.review-post-tooltip').removeClass('in').addClass('out');
            },function(){
                $('.next-post-tooltip').removeClass('in');
                $('.next-post-tooltip').addClass('out');
            });
           }

        var hasScrolled = false;
        var lastScroll = 0;

        $(window).scroll(function() {
            hasScrolled = true;
        });
        var article = $('#single .container .row');
        var articleEnd = article.offset().top + article.height();
        function onScroll(){
            if(hasScrolled){
                var scroll = $(window).scrollTop();
                var sh = $('#single').height();
                var rh = $('.review-post-tooltip').height();
                if(Math.abs(scroll - lastScroll) <= 15) return;
                if(scroll > lastScroll && scroll <= (articleEnd - window.innerHeight)) {
                    //scrollDown
                    if(scroll > $('#featured-post').offset().top + $('#featured-post').height()){
                        $('.navbar').removeClass('nav-show').addClass('nav-hide');
                    }
                } else if(scroll < lastScroll || scroll > (articleEnd - window.innerHeight)) {
                    //scrollUp
                    $('.navbar').removeClass('nav-hide').addClass('nav-show');
                }
                lastScroll = scroll;
            }
        };
        if($('#featured-post').length){
            setInterval(onScroll, 250);
        }
        var pid = $('meta[name=post-id]').attr('content');
        var voto = getCookie('bbox-vote-'+pid);
        if(voto != "") {
            for(var i = 1; i<=voto; i++){
                   $('[data-vote='+i+']').addClass('voted');
            }
        }
        var vote;
        var ajaxUrl = $('meta[name=vote-url]').attr('content');
        $('[data-vote]').click(function(){
            var pid = $(this).attr('data-post-id');
            vote = $(this).attr('data-vote');
            var dataToSend = {action:'post_vote',post_id: pid, nota: vote};
            if(getCookie('bbox-vote-'+pid)!=""){
                dataToSend.again = 'true';
                dataToSend.old_vote = getCookie('bbox-vote-'+pid);
            }
            setCookie('bbox-vote-'+pid, vote, 365);
            voto = vote;
               $('[data-vote]').removeClass('voted');
            for(var i = 1; i<=vote; i++){
                   $('[data-vote='+i+']').addClass('voted');
            }
            setTimeout(function(){
                var tid = $(this).attr('aria-describedby');
                $('#'+tid).html('<div class="tooltip-arrow" style="left: 50%;"></div><div class="tooltip-inner"><p><b>Obrigado!</b></p></div>');
            }.bind(this), 50);
            $.ajax({
                type : "post",
                 dataType : "json",
                 url : ajaxUrl,
                 data : dataToSend,
                 success: function(response) {
                    if(response.type == "success") {
                    }
                    else {
                       $('[data-vote]').removeClass('voted');
                       alert('Erro ao enviar voto, por favor tente novamente mais tarde.');
                       console.log(response);
                    }
                 }
            })
        });

        $('[data-vote]').hover(function(){
            var hoverVote = $(this).attr('data-vote');
               $('[data-vote]').removeClass('voted');
            for(var i = 1; i<=hoverVote; i++){
                   $('[data-vote='+i+']').addClass('voted');
            }
        }, function() {
               $('[data-vote]').removeClass('voted');
            for(var i = 1; i<=voto; i++){
                   $('[data-vote='+i+']').addClass('voted');
            }
        });

    }
    // var btnScrolled = false;
    // $(window).scroll(function() {
    //     btnScrolled = true;
    // });
    // function btnScroll(){
    //     if(btnScrolled){
    //         var scroll = $(window).scrollTop();
    //         if(scroll >= 1) {
    //             $('nav.navbar').removeClass('no-btn');
    //             $('nav.navbar').addClass('has-btn');
    //         } else {
    //             $('nav.navbar').removeClass('has-btn');
    //             $('nav.navbar').addClass('no-btn');
    //         }
    //         btnScrolled = false;
    //     }
    // }
    // setInterval(btnScroll, 500);

    if($('.book-subscribe').length){
        $('.book-subscribe a.email').click(function(){
            $(this).hide();
            $('.sub-form').show();
        });
        $('.book-subscribe a.facebook').click(function(){
            FB.login(function(response){
                if(response.status == 'connected'){
                    FB.api('/me', function(response){
                        if(!response || response.error){
                            alert('Ocorreu um erro, por favor tente novamente mais tarde.');
                        } else {
                            $('.sub-form form input[type=email]').val(response.email);
                            $('.sub-form form').submit();
                        }
                    },{fields: 'name,email'});
                } else {
                    alert('Por favor, conecte-se com o facebook.');
                }
            }, {scope: 'public_profile,email'});
        })
        var ajaxUrl = $('meta[name=vote-url]').attr('content');
        $('.sub-form form').submit(function(e){
            e.preventDefault();
            var uemail = $('.sub-form form input[type=email]').val();
            if(uemail == '') {
                alert('Por favor entre com um e-mail');
                return;
            }
            var dataToSend = {action:'chimp_subscribe',email: uemail};
            $.ajax({
                type : "post",
                 dataType : "json",
                 url : ajaxUrl,
                 data : dataToSend,
                 success: function(response) {
                    console.log(response);
                 }
            });
            setCookie('bbox-blog-newsletter', 'subscribed', 999);
            setTimeout(function(){
                window.location.reload();
            }, 250);
        });
    }

});

function sharer(url, winWidth, winHeight) {
        var winTop = (screen.height / 2) - (winHeight / 2);
        var winLeft = (screen.width / 2) - (winWidth / 2);
        window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width='+winWidth+',height='+winHeight);
}
$('.menu-header').slicknav();