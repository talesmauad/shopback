<?php
 get_header(); ?>
 <?php $posttype = get_queried_object(); ?>
 	<?php $args = array('post_type'=>$posttype->name, 'posts_per_page'=>1, 'meta_query' => array(array('key' => 'destaque', 'value' => true))); ?>
 	<?php $query = new WP_Query($args); ?>
 	<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
		<section id="featured-post" style="background: url(<?php the_post_thumbnail_url('full'); ?>) center center; background-size: cover;">
			<div class="vertical-align">
				<div class="container">
					<div class="row">
						<div class="col-md-5 col-sm-6 col-md-offset-2 post">
							<a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a>
							<div class="meta">
								<div class="author">
									<div class="img-wrapper">
										<?php $foto = get_field('foto_autor'); ?>
										<img src="<?php echo $foto['sizes']['author-thumbnail']; ?>" alt="">
									</div>
									<span><?php the_field('nome_autor'); ?></span>
								</div>
								<a href="<?php the_permalink(); ?>" class="leia-mais inverted pull-left">Leia mais</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
 	<?php endwhile; endif; ?>
	<section id="posts" style="min-height:60vh;">
		<?php $pts = array('post' => 'Artigos', 'especiais' => 'Especiais', 'pocketbook' => 'Pocket Books'); ?>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="col-sm-6">
		        		<form action="<?php get_bloginfo('url'); ?>" method="get">
			        		<div class="input-group search">
			        			<span class="input-group-addon">
			        				<i class="fa fa-search" aria-hidden="true"></i>
			        			</span>
			        			<input type="text" name="s" placeholder="Busca">
			        		</div>
		        		</form>
		        		<div class="lg-spacing"></div>
					</div>
					<div class="col-sm-6 dropdown">
						<button class="btn btn-default btn-block dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					    	<?php echo $pts[$posttype->name]; ?>
					    <span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu col-sm-11" style="margin-left:4.3%" aria-labelledby="dropdownMenu1">
					    <li><a href="<?php bloginfo('url'); ?>/">Todos</a></li>
					    <li><a href="<?php bloginfo('url'); ?>/artigos">Artigos</a></li>
					    <li><a href="<?php bloginfo('url'); ?>/especiais">Especiais</a></li>
					    <li><a href="<?php bloginfo('url'); ?>/pocketbooks">Pocket Books</a></li>
					  </ul>
					</div>
		        	<div class="lg-spacing"></div>
				</div>
			</div>
			<div class="row">
				<ul class="posts col-md-8 col-md-offset-2">
					<?php if($_GET['s']): ?>
						<div class="col-xs-12">
							<h2 class="text-center">Busca: <?php echo $_GET['s']; ?></h2>
						</div>
					<?php endif; ?>
					<?php if($pt): ?>
						<div class="col-xs-12">
							<h2 class="text-center"><?php echo $posttype->labels->singular_name; ?></h2>
						</div>
					<?php endif; ?>
					<?php $pts = array('post' => 'Artigo', 'pocketbook' => 'Pocket Book', 'especiais' => 'Especial'); ?>
					<?php $paged = get_query_var('paged') ? get_query_var('paged') : 1; ?>
					<?php $args = array('post_type'=>$posttype->name, 'posts_per_page'=>6, 'paged' => $paged); ?>
 					<?php if($_GET['s']) $args['s'] = $_GET['s']; ?>
					<?php $query = new WP_Query($args); ?>
					<?php $left = true; ?>
					<?php $boxcount = 0; ?>
					<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
						<?php if($queue && $left==true): ?>
							<?php $pt = get_post_type($qid); ?>
							<div class="col-sm-12">
								<li class="post">
									<div class="img-wrapper">
										<?php echo get_the_post_thumbnail( $qid, 'post-thumbnail'); ?>
										<?php $tempo = get_field('tempo_de_leitura', $qid); ?>
										<?php if($tempo) : ?>
											<span class="leitura"><?php echo $pts[$pt] . ' | '. $tempo; ?> de leitura</span>
										<?php endif; ?>
									</div>
									<div class="text">
										<a href="<?php the_permalink(); ?>" class="title"><?php echo get_the_title($qid); ?></a>
										<div class="text-wrapper">
											<p><?php echo get_the_excerpt($qid); ?></p>
										</div>
										<div class="meta">
											<div class="author">
												<div class="img-wrapper">
													<?php $foto = get_field('foto_autor', $qid); ?>
													<img src="<?php echo $foto['sizes']['author-thumbnail']; ?>" alt="">
												</div>
												<span><?php the_field('nome_autor', $qid); ?></span>
											</div>
											<a href="<?php echo get_permalink($qid); ?>" class="leia-mais pull-sm-right">Leia mais</a>
										</div>
										<span class="clear"></span>
									</div>
								</li>
							</div>
							<?php $boxcount += 2; ?>
							<?php $queue = false; ?>
						<?php endif; ?>
						<?php $pt = get_post_type(); ?>
						<?php $class = 'col-sm-6'; ?>
						<?php if($pt=='pocketbook'): $class = 'col-sm-12'; endif; ?>
						<?php if($left==false && $pt=='pocketbook'): ?>
							<?php $queue = true; ?>
							<?php $qid = get_the_ID(); ?>
						<?php else: ?>
							<div class="<?php echo $class; ?>">
								<li class="post">
									<div class="img-wrapper">
										<?php the_post_thumbnail('post-thumbnail'); ?>
										<?php $tempo = get_field('tempo_de_leitura'); ?>
										<?php if($tempo) : ?>
											<span class="leitura"><?php echo $pts[$pt] . ' | '. $tempo; ?> de leitura</span>
										<?php endif; ?>
									</div>
									<div class="text">
										<a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a>
										<div class="text-wrapper">
											<?php the_excerpt(); ?>
										</div>
										<div class="meta">
											<div class="author">
												<div class="img-wrapper">
													<?php $foto = get_field('foto_autor'); ?>
													<img src="<?php echo $foto['sizes']['author-thumbnail']; ?>" alt="">
												</div>
												<span><?php the_field('nome_autor'); ?></span>
											</div>
											<a href="<?php the_permalink(); ?>" class="leia-mais pull-sm-right">Leia mais</a>
										</div>
										<span class="clear"></span>
									</div>
								</li>
							</div>
							<?php $boxcount += 1; ?>
							<?php if($pt!='pocketboot') $left = !$left; ?>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php else : ?>
						<div class="col-xs-12">
							<p class="text-center" style="margin-top:100px;">Desculpe, nenhum resultado encontrado.</p>
						</div>
					<?php endif; ?>
					<?php if($queue): ?>
						<?php $pt = get_post_type($qid); ?>
						<div class="col-sm-6">
							<li class="post">
								<div class="img-wrapper">
									<?php echo get_the_post_thumbnail( $qid, 'post-thumbnail'); ?>
									<?php $tempo = get_field('tempo_de_leitura', $qid); ?>
									<?php if($tempo) : ?>
										<span class="leitura"><?php echo $pts[$pt] . ' | '. $tempo; ?> de leitura</span>
									<?php endif; ?>
								</div>
								<div class="text">
									<a href="<?php the_permalink(); ?>" class="title"><?php echo get_the_title($qid); ?></a>
									<div class="text-wrapper">
										<p><?php echo get_the_excerpt($qid); ?></p>
									</div>
									<div class="meta">
										<div class="author">
											<div class="img-wrapper">
												<?php $foto = get_field('foto_autor', $qid); ?>
												<img src="<?php echo $foto['sizes']['author-thumbnail']; ?>" alt="">
											</div>
											<span><?php the_field('nome_autor', $qid); ?></span>
										</div>
										<a href="<?php echo get_permalink($qid); ?>" class="leia-mais pull-sm-right">Leia mais</a>
									</div>
									<span class="clear"></span>
								</div>
							</li>
						</div>
						<?php $boxcount += 1; ?>
						<?php $queue = false; ?>
					<?php endif; ?>
					<?php if($boxcount%2==1): ?>
						<?php $args['paged'] += 1; ?>
						<?php $nquery = new WP_Query($args); ?>
						<?php if($nquery->have_posts()): while($nquery->have_posts()): $nquery->the_post(); ?>
							<?php $pt = get_post_type(); ?>
							<?php if($pt!='pocketbook'): ?>
								<div class="col-sm-6">
									<li class="post">
										<div class="img-wrapper">
											<?php the_post_thumbnail('post-thumbnail'); ?>
											<?php $tempo = get_field('tempo_de_leitura'); ?>
											<?php if($tempo) : ?>
												<span class="leitura"><?php echo $pts[$pt] . ' | '. $tempo; ?> de leitura</span>
											<?php endif; ?>
										</div>
										<div class="text">
											<a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a>
											<div class="text-wrapper">
												<?php the_excerpt(); ?>
											</div>
											<div class="meta">
												<div class="author">
													<div class="img-wrapper">
														<?php $foto = get_field('foto_autor'); ?>
														<img src="<?php echo $foto['sizes']['author-thumbnail']; ?>" alt="">
													</div>
													<span><?php the_field('nome_autor'); ?></span>
												</div>
												<a href="<?php the_permalink(); ?>" class="leia-mais pull-sm-right">Leia mais</a>
											</div>
											<span class="clear"></span>
										</div>
									</li>
								</div>
								<?php break; ?>
							<?php endif; ?>
							<?php endwhile; ?>
						<?php endif; ?>
					<?php endif; ?>
					<div class="pagenavi col-xs-12">
						<?php wp_pagenavi(array('query' => $query)); ?>
					</div>
				</ul>
				<span class="clear"></span>
			</div>
		</div>
	</section>
<?php get_footer(); ?>