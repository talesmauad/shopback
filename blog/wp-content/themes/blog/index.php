<?php /*
Template Name: Home
*/
get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero', 'none' ); ?>
	<?php get_template_part( 'global-templates/landingslider', 'none' ); ?>
<?php endif; ?>


<section class="sb-quemsomos" id="quemsomos">
	<div class="home-header-wrapper bg-dark sb-home">
		<div class="home-header-overlay">
			<div class="home-header-shadow">
				<div class="container">
					<div class="home-header-content text-center">
						<div class="row">
							<div class="col-sm-7 mx-auto sb-top-text">
								<h1 class="title text-center sb-main-title">Quer aumentar sua taxa de conversão em <span class="under_orange">35%</span>?</h1>
								<p class="subtitle text text-center sb-main-cta sans">Tecnologia avançada facilmente ativada em 5 minutos e presente em mais de 900 sites pelo mundo, a ShopBack usa inteligência de big-data para reter, re-engajar e recapturar seus usuários transformando-os em clientes.</p>
								<a class="btn btn-default btn-filled-orange" href="#fale" role="button">FALE CONOSCO</a><br/>
								<div class="sb-down-arrow"><i class="fa fa-angle-down"></i></div>
							</div>
						</div>
					</div>
				</div>
				<div class="home-header-down-arrow"><img class="img-responsive" src="<?=get_template_directory_uri()?>/img/down-arrow.png"></div>
			</div>
		</div>
	</div>
</section>

<section class="sb-marcas" id="marcas">
	<ul class="sb-marcas-list">
		<li class="sb-marcas-item"><img src="<?=get_template_directory_uri()?>/img/src/sample-marcas1.png"></li>
		<li class="sb-marcas-item"><img src="<?=get_template_directory_uri()?>/img/src/sample-marcas2.png"></li>
		<li class="sb-marcas-item"><img src="<?=get_template_directory_uri()?>/img/src/sample-marcas3.png"></li>
		<li class="sb-marcas-item"><img src="<?=get_template_directory_uri()?>/img/src/sample-marcas4.png"></li>
		<li class="sb-marcas-item"><img src="<?=get_template_directory_uri()?>/img/src/sample-marcas5.png"></li>
		<li class="sb-marcas-item"><img src="<?=get_template_directory_uri()?>/img/src/sample-marcas6.png"></li>
		<li class="sb-marcas-item"><img src="<?=get_template_directory_uri()?>/img/src/sample-marcas7.png"></li>
	</ul>
</section>

<section class="sb-produtos" id="produtos">
	<h3 class="section-title title-dark">Nossos produtos</h3>
	<div class="row">
		<div class="col-sm-10 mx-auto">
			<div class="row hide-xs">
					<ul class="sb-produtos-list sb-slick-sync">
						<li class="sb-produtos-item item1">On-Site</li>
						<li class="sb-produtos-item item2">Remarketing</li>
						<li class="sb-produtos-item item3">ShopAd</li>
						<li class="sb-produtos-item item4">ShopImobi</li>
						<li class="sb-produtos-item item5">ShopPush</li>
					</ul>
			</div>
			<div class="sb-showcase" id="produtos-showcase">
				<div class="row">
					<div class="col-sm-12">
						<svg class="hide-xs bg-cutter" height="100%" width="100%">
						  <polygon points="15,0 15,1000 340,0" style="fill:white;" />
						</svg>
						<div class="sb-slick-content">
							<div>
								<div class="row">
									<div class="col-sm-5 product-image"><img class="auto-margin" src="<?=get_template_directory_uri()?>/img/sample-computer-home.png"></div>
									<div class="col-sm-7 text-left product-description">
									<img src="<?=get_template_directory_uri()?>/img/rocket.png">
									<h4 class="sans">SOLUÇÕES ON-SITE</h4>
									<h3 class="sans">98% dos usuarios abandonam seu site sem comprar?</h3>
									<span class="sans">Somos diferentes em tudo que você ja viu! Extremamente
									fácil de implementar e ativar, temos um time inteiro pronto
									para fazer ao seu site tudo aquilo que você não tem tempo
									ou braço para fazer, e de quebra ainda aumentamos sua
									conversão em 35%, na média!</span>
									<p><br/><a class="btn btn-default btn-open-white" href="?page_id=8" role="button">+SAIBA MAIS</a></p>
									</div>
								</div>
							</div>
							<div>
								<div class="row">
									<div class="col-sm-5 product-image"><img class="auto-margin" src="<?=get_template_directory_uri()?>/img/sample-computer-home.png"></div>
									<div class="col-sm-7 text-left product-description"><h3>Email Remarketing</h3>
										<b>63% dos carrinhos de compras abandonados não é automaticamente a uma venda perdida.</b><br/>
										<span>Projetado para voltar a envolver os visitantes durante o período de tempo ideal, e-mails reengajamento da Shopback trazer o visitante de volta ao site.</span>
										<p><br/><a class="btn btn-default btn-open-white" href="#" role="button">+SAIBA MAIS</a></p>
									</div>
								</div>
							</div>
							<div><h2>Content 3</h2></div>
							<div><h2>Content 4</h2></div>
							<div><h2>Content 5</h2></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<p><a class="btn btn-default btn-filled-orange" href="#fale" role="button">FALE CONOSCO</a></p>
</section>

<section class="sb-cases" id="clientes">
	<h3 class="section-title">Cases de sucesso</h3>
	<div class="sb-cases-list">
		<div class="sb-cases-item">
			<img class="case-pic" src="<?=get_template_directory_uri()?>/img/jeepwhite.png">
			<h1>Ajudamos a Jeep a aumentar sua taxa de conversão em 47%</h1>
			<p><br/><a class="btn btn-default btn-open-white" href="#" role="button">+SAIBA MAIS</a></p>
		</div>
		<div class="sb-cases-item">
			<img src="<?=get_template_directory_uri()?>/img/src/sample-marcas2.png">
			<h1>Ajudamos a Jeep a aumentar sua taxa de conversão em 47%</h1>
			<p><br/><a class="btn btn-default btn-open-white" href="#" role="button">+SAIBA MAIS</a></p>
		</div>
		<div class="sb-cases-item">
			<img src="<?=get_template_directory_uri()?>/img/src/sample-marcas3.png">
			<h1>Ajudamos a Jeep a aumentar sua taxa de conversão em 47%</h1>
			<p><br/><a class="btn btn-default btn-open-white" href="#" role="button">+SAIBA MAIS</a></p>
		</div>
		<div class="sb-cases-item">
			<img src="<?=get_template_directory_uri()?>/img/src/sample-marcas4.png">
			<h1>Ajudamos a Jeep a aumentar sua taxa de conversão em 47%</h1>
			<p><br/><a class="btn btn-default btn-open-white" href="#" role="button">+SAIBA MAIS</a></p>
		</div>
	</div>
</section>


<section class="sb-marcas-2 hide-xs" id="marcas2">
	<div class="row">
		<div class="col-sm-8 mx-auto" style="padding: 22px;">
			<img src="<?=get_template_directory_uri()?>/img/media.png">
		</div>
	</div>
</section>

<section class="sb-quebracabeca text-center" id="quebracabeca">
<h3 class="section-title title-orange light-orange" style="margin: 10px 0px 0px 0px;">Implementação fácil</h3>
<div class="row">
	<div class="col-sm-4 mx-auto">
		<h2 class="simple">Não quebre a cabeça para implementar</h2>
		<div class="text-center light-orange">Simplesmente copie e cole nossa tag em seu site que em 5 minutos estará funcionando</div>
		<a class="btn btn-default btn-filled-white" href="#fale" role="button">FALE CONOSCO</a>
	</div>
</div>
</section>

<!-- <section class="sb-fale text-center" id="fale">
	<h3 class="section-title title-dark">Assine nossa newsletter e vire um expert em marketing</h3>
	<div class="row mx-auto text-center">
		<div class="text-center mx-auto row-sm-6">
			<input class="news-name bluegray-input" type="text" placeholder="Primeiro nome...">
			<input class="news-mail bluegray-input" type="mail" placeholder="Email..." name="nae">
			<a class="news-send btn btn-default btn-filled-blue smaller" href="#" role="button">ASSINAR</a>
		</div>
	</div>
</section> -->

<section class="sb-blog text-center hide-xs" id="faleBlog">
	<h3 class="section-title title-dark">Últimas do nosso blog</h3>
	<div class="row col-sm-10 mx-auto text-center">
		<div class="col-sm-3 mx-auto text-center">
			<article class="blog-1">
				<div class="blog-image"></div>
				<div class="blog-category">Artigo</div>
				<div class="blog-text text-left">Overlays: melhorando a retenção, reengajamento e conversão do seu site</div>
				<div class="blog-button text-right"><button class="btn btn-default btn-round-open">Ver mais</button></div>
			</article>
		</div>
		<div class="col-sm-3 mx-auto text-center">
			<article class="blog-2">
				<div class="blog-image"></div>
				<div class="blog-category">Artigo</div>
				<div class="blog-text text-left">Links Patrocinados e E-mail Retargeting: Google AdWords + ShopTarget</div>
				<div class="blog-button text-right"><button class="btn btn-default btn-round-open">Ver mais</button></div>
			</article>
		</div>
		<div class="col-sm-3 mx-auto text-center">
			<article class="blog-3">
				<div class="blog-image"></div>
				<div class="blog-category">Artigo</div>
				<div class="blog-text text-left">Fidelizar clientes: Aprenda com exemplos do Esporte e da Música</div>
				<div class="blog-button text-right"><button class="btn btn-default btn-round-open">Ver mais</button></div>
			</article>
		</div>
	</div>
	<a class="btn btn-default btn-open-gray" href="#" role="button">VEJA MAIS</a>
</section>


<section class="sb-news text-center hide-xs" id="faleNews">
	<h3 class="section-title title-dark">Últimas notícias</h3>
	<div class="row col-sm-10 mx-auto text-center">
		<div class="col-sm-3 mx-auto text-center">
			<article class="news-1">
				<div class="news-image"></div>
				<div class="news-category">Artigo</div>
				<div class="news-text text-left">Overlays: melhorando a retenção, reengajamento e conversão do seu site</div>
				<div class="news-button text-right"><button class="btn btn-default btn-round-open">Ver mais</button></div>
			</article>
		</div>
		<div class="col-sm-3 mx-auto text-center">
			<article class="news-2">
				<div class="news-image"></div>
				<div class="news-category">Artigo</div>
				<div class="news-text text-left">Links Patrocinados e E-mail Retargeting: Google AdWords + ShopTarget</div>
				<div class="news-button text-right"><button class="btn btn-default btn-round-open">Ver mais</button></div>
			</article>
		</div>
		<div class="col-sm-3 mx-auto text-center">
			<article class="news-3">
				<div class="news-image"></div>
				<div class="news-category">Artigo</div>
				<div class="news-text text-left">Fidelizar clientes: Aprenda com exemplos do Esporte e da Música</div>
				<div class="news-button text-right"><button class="btn btn-default btn-round-open">Ver mais</button></div>
			</article>
		</div>
	</div>
</section>


<section class="sb-comece" id="comece">
	<h3 class="section-title title-orange">Nunca é tarde demais para crescer suas vendas</h3>
	<div class="row">
		<div class="col-sm-7 mx-auto">
			<h1 class="white simple">Comece a aumentar sua taxa de conversão hoje mesmo</h1>
			<br/>
			<a class="btn btn-default btn-filled-white btn-shadow" href="#" role="button">FALE CONOSCO</a>
		</div>
	</div>
</section>

<div class="wrapper" id="wrapper-index" style="display: none">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<main class="site-main" id="main">

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						?>

					<?php endwhile; ?>

				<?php else : ?>


				<?php endif; ?>

			</main><!-- #main -->

			<!-- The pagination component -->

		</div><!-- #primary -->

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

</script>

<?php get_footer(); ?>
