<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Shopback</title>
	<?php if(is_single()): ?>
		<meta property="og:title" content="<?php the_title(); ?>">
		<meta property="og:type" content="article">
		<meta property="og:url" content="<?php the_permalink(); ?>">
		<meta property="og:site_name" content="bossabox">
		<meta property="og:image" content="<?php the_post_thumbnail_url('full'); ?>">
		<?php $tn_id = get_post_thumbnail_id( $post->ID );
		$img = wp_get_attachment_image_src( $tn_id, 'full' );
		$width = $img[1];
		$height = $img[2]; ?>
		<meta property="og:image:width" content="<?php echo $width; ?>">
		<meta property="og:image:height" content="<?php echo $height; ?>">
		<meta property="og:description" content="<?php echo get_the_excerpt(); ?>">
	<?php else : ?>
		<meta property="og:title" content="Blog">
		<meta property="og:type" content="website">
		<meta property="og:url" content="<?php the_permalink(); ?>">
		<meta property="og:image:width" content="1200">
		<meta property="og:image:height" content="613">
	<?php endif; ?>
	<meta property="fb:app_id" content="1201442006555991">
    <?php wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css?family=Montserrat:200,300,400,400i,500,600,700,800|Source+Sans+Pro:300,400,600,700,900', array(), '1.0.0'); ?>
	<?php wp_enqueue_style('font-awesome', get_bloginfo('template_url').'/css/font-awesome.min.css', array(), '1.0.0'); ?>


	<?php wp_enqueue_style('roboto-slab', 'https://fonts.googleapis.com/css?family=Roboto+Slab:400,700', array(), '1.0.0'); ?>

	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/bootstrap-theme.css">
	<link href='https://fonts.googleapis.com/css?family=Exo:100,200,300,400,600,700,800,900' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/slicknav.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick-theme.css">
	<!-- <link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/shopback.css"> -->
	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/style.css">
	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/style2.css">
	<?php wp_head(); ?>
</head>
<body id="blog">
	<div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar">
		
	
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1201442006555991',
      xfbml      : true,
      version    : 'v2.7'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>


		<header class="header-blog">
			<div class="progress-bar" id="leitura"></div>
				<div class="container cont-head">
					<div class="row">
						<div class="col-md-3 text-center">
							<a href="<?php echo get_site_url(); ?>/..">

								<img src="<?=get_template_directory_uri()?>/img/shopback-logo-gray.png">

							</a>
						</div>
						<div class="col-md-6 text-center">
							<ul class="menu-header">
								<a href="<?php echo get_site_url(); ?>/../quem-somos"><li class="item-menu-header">Quem somos</li></a>
								<a href="<?php echo get_site_url(); ?>/../produtos"><li class="item-menu-header">Produtos</li></a>
								<a href="<?php echo get_site_url(); ?>/../#clientes"><li class="item-menu-header">Clientes</li></a>
								<a href="<?php echo get_site_url(); ?>/../news"><li class="item-menu-header">News</li></a>
								<a href="<?php echo get_site_url(); ?>"><li class="item-menu-header item-header-active">Blog</li></a>
							</ul>
						</div>
						<div class="col-md-3 login text-center">
							<a href="<?php echo get_site_url(); ?>/../login">Login</a>
						</div>
					</div>
				</div>
			<span class="clear"></span>
		</header>
