<?php the_post();
 get_header(); ?>

	<section id="featured-post" style="background: url(<?php the_post_thumbnail_url('full'); ?>) center center; background-size: cover; background-attachment: fixed; background-repeat: no-repeat; background-position-y: 100%;">
		<div class="vertical-align">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-sm-8 post text-center">
						<a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a>
						<div class="meta">
							<div class="author">
								<span>Por <b><?php echo get_field('nome_autor'); ?></b></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="single">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<p class="shop-color"><b><?php the_field('tempo_de_leitura'); ?> de leitura</b></p>

					<?php the_content(); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2" id="comments">
					<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="5"></div>
				</div>
			</div>
		</div>
		<?php if(get_field('anuncio')): ?>
			<?php $posts = get_field('posts_para_anuncio'); ?>
			<?php $post = $posts[array_rand($posts)]; ?>
			<div class="see-also">
				<a href="<?php echo get_permalink($post); ?>">
					<div class="img-container"><?php echo get_the_post_thumbnail($post, 'full'); ?></div>
					<div class="text-container">
						<span class="leia-tambem">Leia também</span>
						<h4><?php echo get_the_title($post); ?></h4>
					</div>
				</a>
			</div>
		<?php endif; ?>
		<!-- <div class="share" data-spy="affix">
			<a href="#" class="sharer" onclick="return false;"><i style="margin-left: -2px;" class="fa fa-share-alt"></i></a>
			<ul>
				<li class="child first"><a href="javascript:sharer('https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>', 520, 350);" title="Compartilhe no Facebook" class="facebook"><i class="fa fa-facebook"></i></a></li>
				<li class="child second"><a href="javascript:sharer('http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&summary=<?php echo get_the_excerpt(); ?>&source=<?php the_permalink(); ?>' ,520,350);" title="Compartilhe no LinkedIn" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
				<li class="child third"><a href="javascript:sharer('https://twitter.com/intent/tweet?source=<?php the_permalink(); ?>&text=<?php the_title(); ?>:<?php the_permalink(); ?>' ,520,350);" title="Compartilhe no Twitter" class="twitter"><i class="fa fa-twitter"></i></a></li>
			</ul>
		</div> -->
	</section>
<?php wp_enqueue_script('see-also', get_bloginfo('template_url').'/js/see-also.js', array('jquery'), '1.0.2'); ?>
<?php get_footer(); ?>
