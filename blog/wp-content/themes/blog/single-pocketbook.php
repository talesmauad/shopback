<?php the_post(); get_header(); ?>

	<?php if(!isset($_COOKIE['bbox-blog-newsletter']) || (isset($_COOKIE['bbox-blog-newsletter']) && $_COOKIE['bbox-blog-newsletter']=='dontshow')): ?>
		<section id="pocketbook-featured" style="margin-bottom: 0;">
			<img src="<?php the_post_thumbnail_url('full'); ?>" alt="" class="bg-blur">
			<div class="pocketbook-cover" style="margin-bottom: 0;">
				<div class="book-logo text-center">
					<i class="icon4-logo-icone"></i>
				</div>
				<div class="vertical-align">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<h5 class="light text-center">Pocket<b>Book</b></h5>
								<h3 class="pocket-title text-center"><?php the_title(); ?></h3>
								<h5 class="light text-center">Por <b><?php the_field('nome_autor'); ?></b></h5>
							</div>
						</div>
					</div>
				</div>
				<div class="book-subscribe">
					<div class="container">
						<div class="row">
							<div class="sub-buttons text-center">
								<a class="book-c2a facebook" href="!#"><i class="icon2-facebook left"></i> Conectar com o Facebook</a>
								<div class="xs-spacing"></div>
								<h5 class="text-center"><small>ou</small></h5>
								<a class="book-c2a email" href="!#"><i class="fa fa-envelope-o left"></i> Conectar com seu E-mail</a>
								<div class="sub-form" style="display: none;">
									<form action="" class="col-md-4 col-md-offset-4">
										<span class="input-wrapper">
											<input id="book-sub-email" type="email" placeholder="Seu e-mail">
											<button type="submit"><i class="icon2-arrow"></i></button>
										</span>
									</form>
								</div>
								<span class="clear"></span>
								<div class="xs-spacing"></div>
								<h5 class="text-center"><small>para ter acesso ao Pocket<b>Book</b></small></h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<span class="clear"></span>
	<?php else: ?>

		<section id="pocketbook-featured">
			<img src="<?php the_post_thumbnail_url('full'); ?>" alt="" class="bg-blur">
			<div class="pocketbook-cover">
				<div class="book-logo text-center">
					<i class="icon4-logo-icone"></i>
				</div>
				<div class="vertical-align">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<h5 class="light text-center">Pocket<b>Book</b></h5>
								<h3 class="pocket-title text-center"><?php the_title(); ?></h3>
								<h5 class="light text-center">Por <b><?php the_field('nome_autor'); ?></b></h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<span class="clear"></span>

		<section id="single" class="pocketbook">

				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2">
							    	<h2>Introdução</h2>
									<?php the_field('introducao'); ?>
						</div>
					</div>
				</div>

			<?php $count = 1; ?>
			<?php if( have_rows('topicos') ):
			 	// loop through the rows of data
			    while ( have_rows('topicos') ) : the_row(); ?>
				<div class="pocket-cap" style="background-image: url(<?php the_sub_field('imagem'); ?>); background-attachment: fixed; background-size: cover;">
					<div class="vertical-align">
						<div class="container">
							<div class="row">
								<div class="col-sm-6 col-sm-offset-3">
							    	<h2 class="title-topic" id="topic<?php echo $count; ?>"><?php the_sub_field('titulo'); ?></h2>
							    	<h4 class="minutos-leitura"><?php the_sub_field('tempo_leitura'); ?> de leitura</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2">
									<?php the_sub_field('conteudo'); ?>
						</div>
					</div>
				</div>

	    	<?php
		        $count++;
		    	endwhile; endif; ?>
		    <div class="container">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="5"></div>
					</div>
				</div>
		    </div>

				<?php if(get_field('anuncio')): ?>
					<?php $posts = get_field('posts_para_anuncio'); ?>
					<?php $post = $posts[array_rand($posts)]; ?>
					<div class="see-also">
						<a href="<?php echo get_permalink($post); ?>">
							<div class="img-container"><?php echo get_the_post_thumbnail($post, 'full'); ?></div>
							<div class="text-container">
								<span class="leia-tambem">Leia também</span>
								<h4><?php echo get_the_title($post); ?></h4>
							</div>
						</a>
					</div>
				<?php endif; ?>
		</section>
	<?php endif; ?>

	<?php wp_enqueue_script('see-also', get_bloginfo('template_url').'/js/see-also.js', array('jquery'), '1.0.2'); ?>
<?php wp_enqueue_script('pocketbook', get_bloginfo('template_url').'/js/pocketbook.js', array('jquery'), '1.0.10', true); ?>
<?php get_footer(); ?>
